import React, {Component} from 'react';
import {connect} from 'react-redux';
import LoginForm from './blocks/UserBlock';
import LinksBlock from "./blocks/links-block/LinksBlock";
import PlaceBlock from "./blocks/PlaceBlock";
import Cart from './cart/Cart';

class SidebarRight extends Component {

    render() {
        return (
            <div className="wrapper">
                {this.props.isAdmin ?  <Cart/> : ''}
                <LoginForm/>
                <LinksBlock/>
                <PlaceBlock/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isAdmin: state.isAdmin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SidebarRight);