import React, {Component} from 'react';
import {connect} from 'react-redux';
import Header from './general/header/Header';
import Footer from './general/footer/Footer';
import AdminPage from './admin/AdminPage';
import User from './User';
import NormalPage from './NormalPage';
import {Route} from 'react-router-dom';
import '../../node_modules/antd/dist/antd.min.css'
import '../scss/General.scss';
import '../scss/Sections.scss'
import '../scss/ant/AntCustomized.scss'
import {categoriesFetchData} from '../redux/actions/categories';
import {websitesFetchData} from '../redux/actions/websites';
import {emplacementsFetchData} from '../redux/actions/emplacements';
import {formatsFetchData} from '../redux/actions/formats';

class App extends Component {

    constructor(props) {
        super(props);
        this.props.fetchCategories();
        this.props.fetchFormats();
        this.props.fetchWebsites();
        this.props.fetchEmplacements();
    }

    render() {
        return (
            <div className={this.props.isAdmin ? "main-container login" : "main-container"}>

                <Header/>
                <User/>

                <section className="main-wrapper container">
                    {/*<Breadcrumb path={this.props.location.pathname}/>*/}
                    {this.props.isAdmin ?<Route path="/" component={AdminPage}/>:''}
                    <Route path="/" component={NormalPage}/>
                </section>
                <Footer/>

            </div>
        );
    }
}

// export default App;
const mapStateToProps = (state) => {
    return {
        user: state.user,
        isAdmin: state.isAdmin,
        pageIsAdmin: state.pageIsAdmin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCategories: () => dispatch(categoriesFetchData()),
        fetchFormats: () => dispatch(formatsFetchData()),
        fetchWebsites: () => dispatch(websitesFetchData()),
        fetchEmplacements: () => dispatch(emplacementsFetchData()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);