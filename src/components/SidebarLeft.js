import React, {Component} from 'react';
import {Route} from 'react-router-dom'
import VerticalMenuBlock from "./blocks/VerticalMenuBlock";
import RecommendedContentsBlock from "./blocks/RecommendedContentsBlock";
import PopularContentsBlock from "./blocks/PopularContentsBlock";
import FilterForm from './content/FilterForm';

class SidebarLeft extends Component {


    render() {
        return (
            <div className="wrapper">

                <Route path="/leaflet" render={() => <FilterForm category="leaflet"/>}/>
                <Route path="/graphics" render={() => <FilterForm category="graphics"/>}/>
                <Route path="/videos" render={() => <FilterForm category="videos"/>}/>
                <Route path="/audios" render={() => <FilterForm category="audios"/>}/>
                <Route path="/ideas" render={() => <FilterForm category="ideas"/>}/>
                <Route path="/journal" render={() => <FilterForm category="journal"/>}/>
                <Route path="/literature" render={() => <FilterForm category="literature"/>}/>
                <Route path="/poster" render={() => <FilterForm category="poster"/>}/>
                <Route path="/" exact render={() => <VerticalMenuBlock/>}/>

                <RecommendedContentsBlock/>
                <PopularContentsBlock/>
            </div>
        );
    }

}

export default SidebarLeft;