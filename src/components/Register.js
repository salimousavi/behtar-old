import React, {Component} from 'react';
import {connect} from 'react-redux';
import {register} from '../redux/actions/user';
import { Redirect } from 'react-router-dom';
import _ from 'lodash';
import { Form, Input, Tooltip, Icon, Button } from 'antd';

const FormItem = Form.Item;

class Register extends Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.props.register(values);
            }
        });
    };

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };

    checkPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('رمز های وارد شده یکسان نمی باشند!');
        } else {
            callback();
        }
    };

    checkConfirm = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    };

    render() {
        if (!_.isEmpty(this.props.user)) {
            return <Redirect to={'/'} />;
        }

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <Form className="has-background-form" onSubmit={this.handleSubmit}>
                <div className="form-item-wrapper">
                    <FormItem
                        {...formItemLayout}
                        label="ایمیل"
                        hasFeedback
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: 'email', message: 'ورودی معتبر نمی باشد!',
                            }, {
                                required: true, message: 'لطفا ایمیل خود را وارد کنید!',
                            }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="رمز عبور"
                        hasFeedback
                    >
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true, message: 'لطفا رمز عبور مورد نظرتان را وارد کنید!',
                            }, {
                                validator: this.checkConfirm,
                            }],
                        })(
                            <Input type="password" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="تایید رمز عبور"
                        hasFeedback
                    >
                        {getFieldDecorator('confirm', {
                            rules: [{
                                required: true, message: 'رمز عبور را دوباره وارد کنید!',
                            }, {
                                validator: this.checkPassword,
                            }],
                        })(
                            <Input type="password" onBlur={this.handleConfirmBlur} />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label={(
                            <span>
              نام مستعار&nbsp;
                                <Tooltip title="این نام به دیگران نشان داده خواهد شد.">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
                        )}
                        hasFeedback
                    >
                        {getFieldDecorator('nickname', {
                            rules: [{ required: true, message: 'لطفا نام مستعار مورد نظرتان را وارد کنید!', whitespace: true }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="شماره همراه"
                        className="phone-number has-prefix left-align"
                    >
                        {getFieldDecorator('phone', {
                            rules: [{ required: true, message: 'لطفا شماره تلفن همراهتان را وارد کنید!' }],
                        })(
                            <Input prefix="+98" />
                        )}
                    </FormItem>
                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" loading={this.props.isLoading} disabled={this.props.isLoading}>
                            {/*{this.props.isLoading ? <Icon type="loading"/> : ''}*/}
                            ثبت نام
                        </Button>
                    </FormItem>
                </div>
                <Icon className="form-background" type="user-add" />
            </Form>
        );
    }
}

const RegistrationForm = Form.create()(Register);


const mapStateToProps = (state) => {
    return {
        user: state.user,
        hasError: state.createUserHasError,
        isLoading: state.createUserIsLoading,
        success: state.createUserSuccess,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        register: (data) => dispatch(register(data)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm);