import React, {Component} from 'react';
import {connect} from 'react-redux';
import {USERS_PATH, BASE_URL} from '../../../tools/Constants';
import {usersFetchData, changeStatusUsers} from "../../../redux/actions/users";
import { Icon, Popconfirm, message} from 'antd';
import {Header} from '../../../tools/Header';

class StatusUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
        }
    }

    changeStatus() {
        this.setState({loading: true});
        if (!this.props.user.active) {
            this.props.changeStatus([this.props.user.id], 'activate')
        }
        else {
            this.props.changeStatus([this.props.user.id], 'block')
        }

    }

    render() {

        return (
            <Popconfirm title={this.props.user.active ? "آیا از مسدود کردن کاربر مطمئن هستید؟" : "آیا از فعال کردن کاربر مطمئن هستید؟"} onConfirm={this.changeStatus.bind(this)} okText="بله"
                        cancelText="لغو">
                    <span className="action">
                        {this.state.error ? <Icon type="frown-o"/>:
                            this.state.loading ?
                                <Icon type="loading"/>
                                : this.props.user.active ?
                                <Icon type="play-circle-o" />
                                : <Icon type="pause-circle-o" />}
                    </span>
            </Popconfirm>
        )
    }
}

const StateToProps = (state) => {
    return {
        isLoading: state.changeStatusUsersIsLoading,
        HasError: state.changeStatusUsersHasError,
    };
};

const DispatchToProps = (dispatch) => {
    return {
        fetchData: () => dispatch(usersFetchData()),
        changeStatus: (ids, type) => dispatch(changeStatusUsers(ids, type)),
    };
};

export default connect(StateToProps, DispatchToProps)(StatusUser);