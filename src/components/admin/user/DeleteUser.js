import React, {Component} from 'react';
import {connect} from 'react-redux';
import {USERS_PATH, BASE_URL} from '../../../tools/Constants';
import {usersFetchData} from "../../../redux/actions/users";
import { Icon, Popconfirm, message} from 'antd';
import {Header} from '../../../tools/Header';

class DeleteUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
        }
    }

    delete() {
        let url = BASE_URL + USERS_PATH;
        this.setState({loading: true});

        fetch(url, Header({
            method: 'DELETE',
            body: JSON.stringify({ids:[this.props.user.id]})
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                message.success('کاربر با موفقیت حذف شد.');
                this.props.fetchData();
            })
            .catch((response) => {
                this.setState({error: true});
                message.error('خطا از سمت سرور');
            });

    }

    render() {
        return (
            <Popconfirm title="آیا از حذف این محتوا مطمئن هستید؟" onConfirm={this.delete.bind(this)} okText="بله"
                        cancelText="لغو">
                    <span className="action">
                        {this.state.error ? <Icon type="frown-o"/>:
                            this.state.loading ?
                                <Icon type="loading"/>
                                : <Icon type="delete"/>}
                    </span>
            </Popconfirm>
        )
    }
}

const StateToProps = (state) => {
    return {
    };
};

const DispatchToProps = (dispatch) => {
    return {
        fetchData: () => dispatch(usersFetchData()),
    };
};

export default connect(StateToProps, DispatchToProps)(DeleteUser);