import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setPageIsAdmin} from "../../../redux/actions/page";
import Loading from "../../Loading";
import Time from '../../Time'
import {usersFetchData, changeStatusUsers, deleteUsers} from "../../../redux/actions/users";
import {Table, Icon, Menu, Button, Dropdown} from 'antd';
import {Link} from 'react-router-dom';
import DeleteUser from './DeleteUser'
import StatusUser from './StatusUser'
import _ from 'lodash'

class AdminUsers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedRows: []
        };
    }

    componentDidMount() {
        this.props.setPageIsAdmin(true);
        this.props.fetchData();
    }

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    activate() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.activate(ids);
    }

    block() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.block(ids);
    }

    delete() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.delete(ids);
    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the users</p>;
        }

        if (this.props.isLoading) {
            return <Loading/>;
        }

        const dataSource = this.props.users;

        const columns = [{
            // title: 'تصویر',
            // dataIndex: 'image',
            // key: 'image',
            // }, {
            title: 'نام',
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => <Link to={"/content/" + record.id + "/" + text}>{text}</Link>,
        }, {
            title: 'نام کاربری',
            dataIndex: 'username',
            key: 'username',
        }, {
            title: 'تاریخ عضویت',
            dataIndex: 'created',
            key: 'created',
            render: (text) => <Time time={text}/>
            ,
        },{
            // title: 'آخرین ورود',
            // dataIndex: 'login',
            // key: 'login',
            // render: (text) => <Time time={text}/>
            // ,
        // },{
            title: 'آخرین دسترسی',
            dataIndex: 'access',
            key: 'access',
            render: (text) => <Time time={text}/>
            ,
        }, {
            title: ' ',
            key: 'action',
            render: (text, record) => (
                <div className="actions">
                    <Link to={"/users/" + record.id + "/edit"} className="action"><Icon type="edit"/></Link>
                    <DeleteUser user={record} />
                    <StatusUser user={record}/>
                </div>
            ),
        }];

        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({
                    selectedRows
                })
            },
        };

        return (
            <div className="admin-users">

                <div className="title">
                    لیست کاربران
                </div>

                <Table rowSelection={rowSelection} dataSource={dataSource} columns={columns}/>
                {_.isEmpty(this.state.selectedRows) ? '' : <Dropdown overlay={
                    <Menu>
                    <Menu.Item>
                        <span onClick={this.activate.bind(this)}>
                        فعال کردن کاربران انتخابی
                        </span>
                    </Menu.Item>
                    <Menu.Item>
                        <span onClick={this.block.bind(this)}>
                        بلاک کردن کاربران انتخابی
                        </span>
                    </Menu.Item>
                    <Menu.Item>
                        <span onClick={this.delete.bind(this)}>
                        حذف کاربران انتخابی
                        </span>
                    </Menu.Item>
                </Menu>} placement="bottomLeft">
                    <Button className="actions-button">عملیات</Button>
                </Dropdown>}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        users: state.users,
        isLoading: state.usersIsLoading,
        hasError: state.usersHasError
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        fetchData: () => dispatch(usersFetchData()),
        activate: (ids, type) => dispatch(changeStatusUsers(ids, 'activate')),
        block: (ids, type) => dispatch(changeStatusUsers(ids, 'block')),
        delete: (ids) => dispatch(deleteUsers(ids)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminUsers);
