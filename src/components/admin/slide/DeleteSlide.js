import React, {Component} from 'react';
import {connect} from 'react-redux';
import {SLIDES_PATH, BASE_URL} from '../../../tools/Constants';
import {slidesFetchData} from "../../../redux/actions/slides";
import { Icon, Popconfirm, message} from 'antd';
import {Header} from '../../../tools/Header';

class DeleteSlide extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
        }
    }

    delete() {
        let url = BASE_URL + SLIDES_PATH;
        this.setState({loading: true});

        fetch(url, Header({
            method: 'DELETE',
            body: JSON.stringify({ids:[this.props.slide.id]})
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                message.success('اسلاید با موفقیت حذف شد.');
                this.props.fetchData(SLIDES_PATH);
            })
            .catch((response) => {
                this.setState({error: true});
                message.error('خطا از سمت سرور');
            });

    }

    render() {
        return (
            <Popconfirm title="آیا از حذف این اسلاید مطمئن هستید؟" onConfirm={this.delete.bind(this)} okText="بله"
                        cancelText="لغو">
                    <span className="action">
                        {this.state.error ? <Icon type="frown-o"/>:
                            this.state.loading ?
                                <Icon type="loading"/>
                                : <Icon type="delete"/>}
                    </span>
            </Popconfirm>
        )
    }
}

const StateToProps = (state) => {
    return {
    };
};

const DispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(slidesFetchData(url)),
    };
};

export default connect(StateToProps, DispatchToProps)(DeleteSlide);