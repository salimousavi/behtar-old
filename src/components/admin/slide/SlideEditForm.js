import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createSlide, slideFetchData} from '../../../redux/actions/slides';
import { Redirect } from 'react-router-dom';
import {getTokenObject} from '../../../tools/Header';
import {BASE_URL, UPLOAD_PATH} from '../../../tools/Constants'
import {Form, Radio, Button, Upload, Icon, Input, Checkbox} from 'antd';
import {setPageIsAdmin} from "../../../redux/actions/page";
import {deleteFiles} from "../../../redux/actions/content";
import Loading from "../../Loading";
import _ from 'lodash';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;

class Slide extends Component {

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    componentDidMount() {
        this.props.setPageIsAdmin(true);
        this.props.getSlide(this.props.match.params.slide_id);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                values.image = values.upload[0].response;

                values.id = this.props.slide.id;
                console.log('Received values of form: ', values);
                this.props.create(values);
            }
        });
    };

    normFile = (e) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    remove(e) {
        this.props.deleteFiles([e.response])
    }

    render() {

        if (this.props.success) {
            return <Redirect to={'/admin/slides'} />;
        }

        const {getFieldDecorator} = this.props.form;
        const formItemLayout = {
            labelCol: {span: 4},
            wrapperCol: {span: 20},
        };

        if (this.props.isLoading || _.isEmpty(this.props.slide)) {
            return <Loading/>;
        }

        return (

            <Form className="has-background-form" onSubmit={this.handleSubmit}>
                <div className="form-item-wrapper">

                    <FormItem
                        {...formItemLayout}
                        label="عنوان"
                    >
                        {getFieldDecorator('title', {
                            initialValue: this.props.slide.title,
                            rules: [{required: true, message: "عنوان اسلاید را وارد نمایید."}],
                        })(
                            <Input placeholder="عنوان اسلاید را وارد نمایید."/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="سرخط"
                    >
                        {getFieldDecorator('headline', {
                            initialValue: this.props.slide.headline,
                            rules: [{required: true, message: "سرخط اسلاید را وارد نمایید."}],
                        })(
                            <Input placeholder="سرخط اسلاید را وارد نمایید."/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="لینک به"
                    >
                        {getFieldDecorator('link', {
                            initialValue: this.props.slide.link,
                        })(
                            <Input placeholder=""/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="وضعیت">
                        {getFieldDecorator('enable', {
                            initialValue: this.props.slide.enable,
                            rules: [{required: true, message: 'آیا اسلاید فعال است؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">فعال</RadioButton>
                                <RadioButton value="0">غیرفعال</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="وب سایت ها"
                        className="websites-form-item">
                        {getFieldDecorator('websites', {
                            initialValue: this.props.slide.websites,
                            rules: [{required: true, message: 'لطفا وبسایت های مورد نظرتان را انتخاب کنید!'}],
                        })(
                            <CheckboxGroup options={this.props.websites} key="websites"/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="تصویر"
                    >
                        {getFieldDecorator('upload', {
                            valuePropName: 'file',
                            getValueFromEvent: this.normFile,
                            getValueFromLoad: this.normFile,
                            initialValue: this.props.slide.image_edit,
                            rules: [{required: true, message: 'تصویر اسلاید را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="image/*" listType="picture" action={BASE_URL+UPLOAD_PATH} defaultFileList={[...this.props.slide.image_edit]} headers={getTokenObject()}  onRemove={this.remove.bind(this)}>
                                <Button>
                                    <Icon type="upload"/>بارگذاری
                                </Button>
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        wrapperCol={{ offset: 4}}
                    >
                        <Button type="primary" htmlType="submit" loading={this.props.createIsLoading} >
                            تایید
                            {this.props.success}
                        </Button>
                    </FormItem>
                </div>
                <Icon className="form-background" type="folder-add" />
            </Form>
        );
    }
}

const SlideEditForm = Form.create()(Slide);


const mapStateToProps = (state) => {
    return {
        slide: state.slide,
        hasError: state.slideHasError,
        isLoading: state.slideIsLoading,
        createHasError: state.createSlideHasError,
        createIsLoading: state.createSlideIsLoading,
        success: state.createSlideSuccess,
        websites: state.websites,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        create: (data) => dispatch(createSlide(data, 'PUT')),
        getSlide: (id) => dispatch(slideFetchData(id)),
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        deleteFiles: (data) => dispatch(deleteFiles(data)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(SlideEditForm);
