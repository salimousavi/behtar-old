import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createSlide} from '../../../redux/actions/slides';
import { Redirect } from 'react-router-dom';
import {getTokenObject} from '../../../tools/Header';
import {BASE_URL, UPLOAD_PATH} from '../../../tools/Constants'
import {Form, Radio, Button, Upload, Icon, Input, Checkbox} from 'antd';
import {setPageIsAdmin} from "../../../redux/actions/page";
import {deleteFiles} from "../../../redux/actions/content";
const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;

class Slide extends Component {

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    componentDidMount() {
        this.props.setPageIsAdmin(true);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                values.image = values.upload[0].response;

                let result = Object.assign({}, values);
                delete result.upload;
                console.log('Received values of form: ', values, result);
                this.props.create(values);
            }
        });
    };

    normFile = (e) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    remove(e) {
        this.props.deleteFiles([e.response])
    }

    render() {

        if (this.props.success) {
            return <Redirect to={'/admin/slides'} />;
        }

        const {getFieldDecorator} = this.props.form;
        const formItemLayout = {
            labelCol: {span: 4},
            wrapperCol: {span: 20},
        };

        return (

            <Form className="has-background-form" onSubmit={this.handleSubmit}>
                <div className="form-item-wrapper">

                    <FormItem
                        {...formItemLayout}
                        label="عنوان"
                    >
                        {getFieldDecorator('title', {
                            rules: [{required: true, message: "عنوان اسلاید را وارد نمایید."}],
                        })(
                            <Input placeholder="عنوان اسلاید را وارد نمایید."/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="سرخط"
                    >
                        {getFieldDecorator('headline', {
                            rules: [{required: true, message: "سرخط اسلاید را وارد نمایید."}],
                        })(
                            <Input placeholder="سرخط اسلاید را وارد نمایید."/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="لینک به"
                    >
                        {getFieldDecorator('link')(
                            <Input placeholder=""/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="وضعیت">
                        {getFieldDecorator('enable', {
                            rules: [{required: true, message: 'آیا اسلاید فعال است؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">فعال</RadioButton>
                                <RadioButton value="0">غیرفعال</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="وب سایت ها"
                        className="websites-form-item">
                        {getFieldDecorator('websites', {
                            rules: [{required: true, message: 'لطفا وبسایت های مورد نظرتان را انتخاب کنید!'}],
                        })(
                            <CheckboxGroup options={this.props.websites} key="websites"/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="تصویر"
                    >
                        {getFieldDecorator('upload', {
                            valuePropName: 'file',
                            getValueFromEvent: this.normFile,
                            rules: [{required: true, message: 'تصویر اسلاید را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="image/*" listType="picture" action={BASE_URL+UPLOAD_PATH} headers={getTokenObject()}  onRemove={this.remove.bind(this)}>
                                <Button>
                                    <Icon type="upload"/>بارگذاری
                                </Button>
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        wrapperCol={{ offset: 4}}
                    >
                        <Button type="primary" htmlType="submit"  disabled={this.props.isLoading} loading={this.props.isLoading} >
                            تایید
                            {this.props.success}
                        </Button>
                    </FormItem>
                </div>
                <Icon className="form-background" type="folder-add" />
            </Form>
        );
    }
}

const SlideForm = Form.create()(Slide);


const mapStateToProps = (state) => {
    return {
        hasError: state.createSlideHasError,
        isLoading: state.createSlideIsLoading,
        success: state.createSlideSuccess,
        websites: state.websites,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        create: (data) => dispatch(createSlide(data)),
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        deleteFiles: (data) => dispatch(deleteFiles(data)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(SlideForm);
