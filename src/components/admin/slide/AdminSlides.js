import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Table, Icon, Menu, Button, Dropdown, Alert } from 'antd';
import { DragDropContext, DragSource, DropTarget } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import update from 'immutability-helper';
import {setPageIsAdmin} from "../../../redux/actions/page";
import {slidesFetchData, deleteSlides, saveChangesSlides} from "../../../redux/actions/slides";
import _ from 'lodash'
import {Link} from 'react-router-dom';
import DeleteSlide from './DeleteSlide'
import SlideForm from './SlideForm'

function dragDirection(
    dragIndex,
    hoverIndex,
    initialClientOffset,
    clientOffset,
    sourceClientOffset,
) {
    const hoverMiddleY = (initialClientOffset.y - sourceClientOffset.y) / 2;
    const hoverClientY = clientOffset.y - sourceClientOffset.y;
    if (dragIndex < hoverIndex && hoverClientY > hoverMiddleY) {
        return 'downward';
    }
    if (dragIndex > hoverIndex && hoverClientY < hoverMiddleY) {
        return 'upward';
    }
}

let BodyRow = (props) => {
    const {
        isOver,
        connectDragSource,
        connectDropTarget,
        moveRow,
        dragRow,
        clientOffset,
        sourceClientOffset,
        initialClientOffset,
        ...restProps
    } = props;
    const style = { ...restProps.style, cursor: 'move' };

    let className = restProps.className;
    if (isOver && initialClientOffset) {
        const direction = dragDirection(
            dragRow.index,
            restProps.index,
            initialClientOffset,
            clientOffset,
            sourceClientOffset
        );
        if (direction === 'downward') {
            className += ' drop-over-downward';
        }
        if (direction === 'upward') {
            className += ' drop-over-upward';
        }
    }

    return connectDragSource(
        connectDropTarget(
            <tr
                {...restProps}
                className={className}
                style={style}
            />
        )
    );
};

const rowSource = {
    beginDrag(props) {
        return {
            index: props.index,
        };
    },
};

const rowTarget = {
    drop(props, monitor) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return;
        }

        // Time to actually perform the action
        props.moveRow(dragIndex, hoverIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex;
    },
};

BodyRow = DropTarget('row', rowTarget, (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    sourceClientOffset: monitor.getSourceClientOffset(),
}))(
    DragSource('row', rowSource, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        dragRow: monitor.getItem(),
        clientOffset: monitor.getClientOffset(),
        initialClientOffset: monitor.getInitialClientOffset(),
    }))(BodyRow)
);


class DragSortingTable extends Component {


    constructor(props) {
        super(props);
        this.state = {
            selectedRows: [],
            data: [],
            change: false,
            visible: true,
        };
    }

    components = {
        body: {
            row: BodyRow,
        },
    };

    componentDidMount() {
        this.props.setPageIsAdmin(true);
        this.props.fetchData();
    }

    componentWillReceiveProps(nextProps) {
        if ( !_.isEmpty(nextProps.slides)) {
            this.setState({
                data: nextProps.slides,
            })
        }
    }

    enable(id) {
        let index = this.state.data.findIndex((el) => {return id === el.id});
        let {data} = this.state;
        data[index].enable = !data[index].enable;
        this.setState({data, change: true});
    }

    delete() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.delete(ids);
    }

    saveData() {
        let {data} = this.state;
        let result = data.map((el, i) => {
            return {
                weight: i,
                id: el.id,
                enable: el.enable,
            }
        });

        this.props.save(result)
        this.setState({
            change: false
        })
    }

    moveRow = (dragIndex, hoverIndex) => {
        this.setState({change: true});
        const { data } = this.state;
        const dragRow = data[dragIndex];

        this.setState(
            update(this.state, {
                data: {
                    $splice: [[dragIndex, 1], [hoverIndex, 0, dragRow]],
                },
            }),
        );
    };

    render() {

        const columns = [
            {
                title: 'عنوان',
                dataIndex: 'title',
                key: 'title',
                render: (text, record) => <Link to={"/comment/" + record.id + "/" + text}>{text}</Link>,
            }, {
                title: 'وضعیت',
                dataIndex: 'enable',
                key: 'enable',
                className: 'status-wrapper',
                render: (text, record) => (
                    <div className="" onClick={() => this.enable(record.id)}>{text == 1 ? <span className="status enable">فعال</span> : <span className="status disable">غیرفعال</span>}</div>),
            }, {
                title: ' ',
                key: 'action',
                render: (text, record) => (
                    <div className="actions">
                        <Link to={"/admin/slides/" + record.id + "/edit"} className="action"><Icon type="edit"/></Link>
                        <DeleteSlide slide={record}/>
                    </div>
                ),
            }];

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the slides</p>;
        }

        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({
                    selectedRows
                })
            },
        };

        return (
            <div>
                <div className="header">
                    <span className="title">
                        لیست اسلایدها
                    </span>
                    <Button className="button add-button" type="dashed">

                        <Link  to="/admin/slides/add">
                            <Icon type="plus"/>
                            افزودن اسلاید
                        </Link>
                    </Button>
                </div>

                <div className="buttons">

                    {this.state.change ?


                        <Alert className="save-changes"
                        description={
                        <span>
                                <span className="notif">
                                    تغییرات شما هنوز ذخیره نشده است. برای ذخیره تغییرات
                                </span>
                                <span className='temp-button' type="" onClick={this.saveData.bind(this)}>
اینجا
                                </span>
                        را کلیک کنید.
                            </span>}
                        type="warning"
                        showIcon
                        />
                        : ''}
                </div>



                <Table
                    rowSelection={rowSelection}
                    loading={this.props.isLoading}
                    columns={columns}
                    dataSource={this.state.data}
                    components={this.components}
                    onRow={(record, index) => ({
                        index,
                        moveRow: this.moveRow,
                    })}
                    expandedRowRender={record => <p style={{ margin: 0 }}>{record.headline}</p>}
                    size="middle"
                />
                {_.isEmpty(this.state.selectedRows) ? '' : <Dropdown overlay={
                    <Menu>
                        <Menu.Item>
                        <span onClick={this.delete.bind(this)}>
                        حذف پیام‌های انتخابی
                        </span>
                        </Menu.Item>
                    </Menu>} placement="bottomCenter">
                    <Button className="actions-button">عملیات</Button>
                </Dropdown>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        slides: state.slides,
        hasError: state.slidesHasError,
        isLoading: state.slidesIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        fetchData: () => dispatch(slidesFetchData()),
        delete: (ids) => dispatch(deleteSlides(ids)),
        save: (data) => dispatch(saveChangesSlides(data)),
    };
};

const AdminSlides = DragDropContext(HTML5Backend)(DragSortingTable);

export default connect(mapStateToProps, mapDispatchToProps)(AdminSlides);