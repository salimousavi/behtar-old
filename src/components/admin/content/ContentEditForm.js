import React, {Component} from 'react'
import {connect} from 'react-redux';
import {updateContent, contentFetchData, deleteFiles} from '../../../redux/actions/content';
import {Redirect} from 'react-router-dom';
import _ from 'lodash';
import {TYPES, BASE_URL, UPLOAD_PATH, CONTENTS_PATH} from '../../../tools/Constants';
import {getTokenObject} from '../../../tools/Header';
import {
    Form, Select, InputNumber, Radio,
    Button, Upload, Icon, Input, Checkbox, TimePicker
} from 'antd';
import moment from 'moment'
import Loading from "../../Loading";
import {setPageIsAdmin} from "../../../redux/actions/page";

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const {TextArea} = Input;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
};

class ContentEdit extends Component {


    componentWillMount() {
        this.props.setPageIsAdmin(true);
        this.props.getContent(CONTENTS_PATH + "/" + this.props.match.params.content_id);
    }

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                console.log('Received values of form: ', values);

                let result = Object.assign({}, values);
                result.image = values.image[0].response;
                if (!_.isEmpty(values.file)) {
                    result.file = values.file.map(f => {return f.response});
                }
                if (!_.isEmpty(values.video)) {
                    result.video = values.video[0].response;
                }
                if (!_.isEmpty(values.audio)) {
                    result.audio = values.audio[0].response;
                }

                if (result.duration !== undefined) {
                    result.duration = result.duration.format('HH:mm:ss');
                }

                result.id = this.props.content.id;
                this.props.sendData(result);

            }
        });
    };

    remove(e) {
        this.props.deleteFiles([e.response])
    }

    getFieldDecorator = this.props.form.getFieldDecorator;
    getFieldValue = this.props.form.getFieldValue;

    selectComponent(name) {
        switch (name) {
            case 'Colorful':
                return (
                    <FormItem
                        {...formItemLayout} label="نوع" key="colorful">
                        {this.getFieldDecorator('colorful', {
                            initialValue: this.props.content.colorful,
                            rules: [{required: true, message: 'فایل شما سیاه سفید است یا رنگی؟!'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">رنگی</RadioButton>
                                <RadioButton value="0">سیاه سفید</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Duration':
                return (
                    <FormItem
                        {...formItemLayout} label="مدت زمان" key="duration">
                        {this.getFieldDecorator('duration', {
                            initialValue: moment(moment.utc(this.props.content.duration*1000).format('HH:mm:ss'), 'HH:mm:ss'),
                            rules: [{required: true, message: 'مدت زمان فایل چقدر است؟'}],
                        })(
                            <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} placeholder="مدت زمان"/>
                        )}
                    </FormItem>
                );

            case 'Expensive':
                return (
                    <FormItem
                        {...formItemLayout} label="نوع" key="expensive">
                        {this.getFieldDecorator('expensive', {
                            initialValue: this.props.content.expensive,
                            rules: [{required: true, message: 'اده شما گران است یا ارزان؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">گران</RadioButton>
                                <RadioButton value="0">ارزان</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Format':
                return (
                    <FormItem
                        {...formItemLayout} label="قالب" key="format">
                        {this.getFieldDecorator('format', {
                            initialValue: this.props.content.format,
                            rules: [{required: true, message: 'قالب فایل بارگذاری شده چیست؟'}],
                        })(
                            <Select style={{width: 120}}>
                                {
                                    this.props.formats.map(function (el, i) {
                                        return <Option value={el.value} key={i}>{el.label}</Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>
                );

            case 'Implemented':
                return (
                    <FormItem
                        {...formItemLayout} label="وضعیت" key="implemented">
                        {this.getFieldDecorator('implemented', {
                            initialValue: this.props.content.implemented,
                            rules: [{required: true, message: 'آیا ایده شما تا به حال اجرا شده است؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1"> اجرا شده</RadioButton>
                                <RadioButton value="0"> اجرا نشده</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Layer':
                return (
                    <FormItem
                        {...formItemLayout} label="وضعیت" key="layer">
                        {this.getFieldDecorator('layer', {
                            initialValue: this.props.content.layer,
                            rules: [{required: true, message: 'فایل بارگذاری شده لایه باز است یا تخت؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">لایه باز</RadioButton>
                                <RadioButton value="0">تخت</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Pages':
                return (
                    <FormItem
                        {...formItemLayout} label="تعداد صفحات" key="pages">
                        {this.getFieldDecorator('pages', {
                            initialValue: this.props.content.pages,
                            rules: [{required: true, message: 'تعداد صفحات را وارد کنید!'}],
                        })(
                            <InputNumber min={1} max={1000}/>
                        )}
                    </FormItem>
                );

            case 'Size':
                return (
                    <div key="size">
                        <FormItem
                            {...formItemLayout} label="عرض" key="width">
                            {this.getFieldDecorator('width', {
                                initialValue: this.props.content.width,
                                rules: [{required: true, message: 'عرض را وارد کنید!'}],
                            })(
                                <InputNumber min={1}/>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout} label="ارتفاع" key="height">
                            {this.getFieldDecorator('height', {
                                initialValue: this.props.content.height,
                                rules: [{required: true, message: 'ارتفاع را وارد کنید!'}],
                            })(
                                <InputNumber min={1}/>
                            )}
                        </FormItem>
                    </div>
                );

            case 'Video':
                return (
                    <FormItem key='video'
                              {...formItemLayout}
                              label="فایل ویدیویی"
                              className="file-form-item">
                        {this.getFieldDecorator('video', {
                            valuePropName: 'video',
                            getValueFromEvent: this.normFile,
                            initialValue: this.props.content.video,
                            rules: [{required: true, message: 'ویدیوی موردنظر را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="video/*" listType="file" action={BASE_URL+UPLOAD_PATH} defaultFileList={[...this.props.content.video]} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                {_.size(this.getFieldValue('video')) === 0 ? <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button> : '' }
                            </Upload>
                        )}
                    </FormItem>
                );

            case 'Audio':
                return (
                    <FormItem key='audio'
                              {...formItemLayout}
                              label="فایل صوتی"
                              className="file-form-item">
                        {this.getFieldDecorator('audio', {
                            valuePropName: 'audio',
                            getValueFromEvent: this.normFile,
                            initialValue: this.props.content.audio,
                            rules: [{required: true, message: 'صوت موردنظر را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" listType="file" accept="audio/*" action={BASE_URL+UPLOAD_PATH} defaultFileList={[...this.props.content.audio]} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                {_.size(this.getFieldValue('audio')) === 0 ? <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button> : '' }
                            </Upload>
                        )}
                    </FormItem>
                );


            default:
                return;
        }
    }

    getFormComponents() {

        let type = TYPES.find((el) => { return el.id == this.props.content.type });
        let fields = [];
        type.fields.forEach((el) => {
            fields.push(this.selectComponent(el));
        });

        return fields;
    }

    normFile = (e) => {
        if (Array.isArray(e)) {
            return e;
        }

        return e && e.fileList;
    };

    render() {

        if (this.props.contentHasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.contentIsLoading || _.isEmpty(this.props.content)) {
            return <Loading/>;
        }

        if (this.props.success) {
            return <Redirect to={'/content/' + this.props.content.id}/>;
        }

        let message = "";
        if (this.props.hasError) {
            message = "<p>Sorry! There was an error sending the package.</p>";
        }

        const {getFieldDecorator, getFieldValue} = this.props.form;
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 20 },
        };

        let content = this.props.content;
        // console.log(_.size(getFieldValue('image')), 'bahbhabh');
        return (
            <Form className="has-background-form" onSubmit={this.handleSubmit}>
                <div className="form-item-wrapper">
                    <span dangerouslySetInnerHTML={{__html: message}}></span>

                    <FormItem
                        {...formItemLayout}
                        label="عنوان">
                        {getFieldDecorator('title', {
                            initialValue: content.name,
                            rules: [{required: true, message: 'عنوان موردنظر را وارد کنید!'}],
                        })(
                            <Input placeholder="عنوان محتوا را وارد کنید."/>
                        )}
                    </FormItem>

                    {this.getFormComponents()}

                    <FormItem
                        {...formItemLayout}
                        label="تصویر"
                    >
                        {getFieldDecorator('image', {
                            valuePropName: 'image',
                            getValueFromEvent: this.normFile,
                            getValueFromLoad: this.normFile,
                            initialValue: this.props.content.image_edit,
                            rules: [{required: true, message: 'تصویر موردنظر را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="image/*" listType="picture" action={BASE_URL+UPLOAD_PATH} defaultFileList={[...this.props.content.image_edit]} headers={getTokenObject()}  onRemove={this.remove.bind(this)}>
                                {_.size(getFieldValue('image')) === 0 ? <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button> : '' }
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="فایل"
                    >
                        {getFieldDecorator('file', {
                            valuePropName: 'file',
                            getValueFromEvent: this.normFile,
                            initialValue: this.props.content.file,
                        })(
                            <Upload name="file" listType="file" action={BASE_URL+UPLOAD_PATH} defaultFileList={[...this.props.content.file]} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button>
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="دسته بندی"
                        key="categories"
                        className="categories-form-item">
                        {this.getFieldDecorator('categories', {
                            initialValue: this.props.content.categories,
                            rules: [{required: true, message: 'دسته‌بندی محتوا را انتخاب کنید.'}],
                        })(
                            <Select style={{width: 120}}>
                                {
                                    this.props.categories.map(function (el, i) {
                                        return <Option value={el.value} key={i}>{el.label}</Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="   وب سایت ها"
                    >
                        {getFieldDecorator('websites', {
                            initialValue: content.websites,
                            rules: [{required: true, message: 'لطفا وبسایت های مورد نظرتان را انتخاب کنید!'}],
                        })(
                            <CheckboxGroup options={this.props.websites} key="websites"/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="محل استفاده"
                    >
                        {getFieldDecorator('emplacements', {
                            initialValue: content.emplacements,
                            rules: [{required: true, message: 'این محتوا در چه مکان هایی قابل استفاده است؟'}],
                        })(
                            <CheckboxGroup options={this.props.emplacements} key="emplacements"/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="متن"
                    >
                        {getFieldDecorator('description', {
                            initialValue: content.description,
                            rules: [{required: true, message: 'متن محتوا را وارد کنید.'}],
                        })(
                            <TextArea placeholder="متن محتوا را وارد کنید." autosize={{ minRows: 4 }} />
                        )}
                    </FormItem>

                    <FormItem
                        wrapperCol={{ offset: 4}}

                    >
                        <Button type="primary" htmlType="submit" disabled={this.props.isLoading} loading={this.props.isLoading} >
                            تایید
                        </Button>
                    </FormItem>
                </div>
                <Icon className="form-background" type="file-add" />
            </Form>
        );
    }
}

function mapStateToProps(state) {
    return {
        hasError: state.createContentHasError,
        isLoading: state.createContentIsLoading,
        success: state.createContentSuccess,
        content: state.content,
        contentIsLoading: state.contentIsLoading,
        contentHasError: state.contentHasError,
        formats: state.formats,
        formatsHasError: state.formatsHasError,
        formatsIsLoading: state.formatsIsLoading,
        websites: state.websites,
        emplacements: state.emplacements,
        categories: state.categories,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        sendData: (data) => dispatch(updateContent(data)),
        getContent: (url) => dispatch(contentFetchData(url)),
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        deleteFiles: (data) => dispatch(deleteFiles(data)),
    }
}

const ContentEditForm = Form.create()(ContentEdit);

export default connect(mapStateToProps, mapDispatchToProps)(ContentEditForm);