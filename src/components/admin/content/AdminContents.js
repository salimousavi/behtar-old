import React, {Component} from 'react';
import {connect} from 'react-redux';
import {CONTENTS_PATH, TYPES} from '../../../tools/Constants';
import {setPageIsAdmin} from "../../../redux/actions/page";
import Loading from "../../Loading";
import Time from '../../Time'
import {contentsFetchData, deleteContents, changeStatusContents} from "../../../redux/actions/contents";
import {Table, Icon, Menu, Button, Dropdown} from 'antd';
import '../../../scss/admin/AdminContents.scss'
import {Link} from 'react-router-dom';
import DeleteContent from './DeleteContent'
import _ from 'lodash'

class AdminContents extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedRows: []
        };
    }

    componentDidMount() {
        this.props.setPageIsAdmin(true);
        this.props.fetchData(CONTENTS_PATH);
    }

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    trim(inputString, maxLength) {

        let trimmedString = inputString.substr(0, maxLength);

        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));

        return trimmedString;
    }

    publish() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.publish(ids);
    }

    unpublish() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.unpublish(ids);
    }

    delete() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.delete(ids);
    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.isLoading) {
            return <Loading/>;
        }

        const dataSource = this.props.contents;

        const columns = [{
            // title: 'تصویر',
            // dataIndex: 'image',
            // key: 'image',
            // }, {
            title: 'عنوان',
            dataIndex: 'name',
            key: 'name',
            render: (text, record) => <Link to={"/content/" + record.id + "/" + text}>{text}</Link>,
        }, {
            title: 'نویسنده',
            dataIndex: 'author',
            key: 'author',
            render: (text) => <span>{text}</span>
            ,
        }, {
            title: 'نوع',
            dataIndex: 'type',
            key: 'type',
            render: (type) => <span>{TYPES.find((el) => { return el.id == type }).name}</span>
            ,
        }, {
            title: 'تاریخ',
            dataIndex: 'time',
            key: 'time',
            render: (text) => <Time time={text}/>
            ,
        }, {
            title: 'وضعیت',
            dataIndex: 'status',
            key: 'status',
            render: (status) => <span>{status === '1' ? 'منتشرشده' : 'منتشر‌نشده'}</span>
            ,
        },{
            title: 'تعداد پیام',
            dataIndex: 'comment_count',
            key: 'comment_count',
            render: (comment_count) => <span>{comment_count}</span>
            ,
        }, {
            title: ' ',
            key: 'action',
            render: (text, record) => (
                <div className="actions">
                    <Link to={"/content/" + record.id + "/edit"} className="action"><Icon type="edit"/></Link>
                    <DeleteContent content={record}/>
                </div>
            ),
        }];

        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({
                    selectedRows
                })
            }
        };

        return (
            <div className="admin-contents">
                <div className="title">
                    لیست محتوا
                </div>
                <Table rowSelection={rowSelection} dataSource={dataSource} columns={columns}/>
                {_.isEmpty(this.state.selectedRows) ? '' : <Dropdown overlay={
                    <Menu>
                        <Menu.Item>
                        <span onClick={this.delete.bind(this)}>
                        حذف محتواهای انتخابی
                        </span>
                        </Menu.Item>
                        <Menu.Item>
                        <span onClick={this.publish.bind(this)}>
                       انتشار محتواهای انتخابی
                        </span>
                        </Menu.Item>
                        <Menu.Item>
                        <span onClick={this.unpublish.bind(this)}>
                        عدم انتشار محتواهای انتخابی
                        </span>
                        </Menu.Item>
                    </Menu>} placement="bottomCenter">
                    <Button className="actions-button">عملیات</Button>
                </Dropdown>}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        contents: state.contents,
        hasError: state.contentsHasError,
        isLoading: state.contentsIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        fetchData: (url) => dispatch(contentsFetchData(url)),
        publish: (ids, type) => dispatch(changeStatusContents(ids, 'publish')),
        unpublish: (ids, type) => dispatch(changeStatusContents(ids, 'unpublish')),
        delete: (ids) => dispatch(deleteContents(ids)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminContents);
