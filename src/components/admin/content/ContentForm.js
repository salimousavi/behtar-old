import React, {Component} from 'react'
import {connect} from 'react-redux';
import {createContent, deleteFiles} from '../../../redux/actions/content';
import {Redirect} from 'react-router-dom';
import _ from 'lodash';
import {websitesFetchData} from '../../../redux/actions/websites';
import {emplacementsFetchData} from '../../../redux/actions/emplacements';
import {TYPES, WEBSITES_PATH, EMPLACEMENTS_PATH, FORMATS_PATH, BASE_URL, UPLOAD_PATH} from '../../../tools/Constants';
import {getTokenObject} from '../../../tools/Header';

import {formatsFetchData} from '../../../redux/actions/formats';
// import EditableTagGroup from './tags';
import {
    Form, Select, InputNumber, Radio,
    Button, Upload, Icon, Input, Checkbox, TimePicker,
} from 'antd';
import moment from 'moment'
import {setPageIsAdmin} from "../../../redux/actions/page";
// import CustomizedForm from './Ali'

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const {TextArea} = Input;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
};

class Content extends Component {

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    componentDidMount() {
        this.props.fetchFormats(FORMATS_PATH);
        this.props.fetchWebsites(WEBSITES_PATH);
        this.props.fetchEmplacements(EMPLACEMENTS_PATH);
        this.props.setPageIsAdmin(true);
    }


    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            console.log(e, 'isArray');
            return e;
        }
        console.log(e, 'list');

        return e && e.fileList;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                let result = Object.assign({}, values);
                result.image = values.image[0].response;
                if (!_.isEmpty(values.file)) {
                    result.file = values.file.map(f => {return f.response});
                }
                if (!_.isEmpty(values.video)) {
                    result.video = values.video[0].response;
                }
                if (!_.isEmpty(values.audio)) {
                    result.audio = values.audio[0].response;
                }

                if (result.duration !== undefined) {
                    result.duration = result.duration.format('HH:mm:ss');
                }

                let type = this.props.match.params.type;

                type = TYPES.find((el) => { return el.type === type });

                result.type = type.id;

                console.log('Received values of form: ', values, result);
                this.props.sendData(result);

            }
        });
    };

    remove(e) {
        this.props.deleteFiles([e.response])
    }

    getFieldDecorator = this.props.form.getFieldDecorator;
    getFieldValue = this.props.form.getFieldValue;

    selectComponent(name) {
        switch (name) {
            case 'Colorful':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="نوع"
                        key="colorful"
                        className="colorful-form-item">
                        {this.getFieldDecorator('colorful', {
                            rules: [{required: true, message: 'فایل شما سیاه سفید است یا رنگی؟!'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">رنگی</RadioButton>
                                <RadioButton value="0">سیاه سفید</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Duration':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="مدت زمان"
                        key="duration"
                        className="duration-form-item">
                        {this.getFieldDecorator('duration', {
                            rules: [{required: true, message: 'مدت زمان فایل چقدر است؟'}],
                        })(
                            <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} placeholder="مدت زمان"/>
                        )}
                    </FormItem>
                );

            case 'Expensive':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="نوع"
                        key="expensive"
                        className="expensive-form-item">
                        {this.getFieldDecorator('expensive', {
                            rules: [{required: true, message: 'اده شما گران است یا ارزان؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">گران</RadioButton>
                                <RadioButton value="0">ارزان</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Format':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="قالب"
                        key="format"
                        className="format-form-item">
                        {this.getFieldDecorator('format', {
                            rules: [{required: true, message: 'قالب فایل بارگذاری شده چیست؟'}],
                        })(
                            <Select style={{width: 120}}>
                                {
                                    this.props.formats.map(function (el, i) {
                                        return <Option value={el.value} key={i}>{el.label}</Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>
                );

            case 'Implemented':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="وضعیت"
                        key="implemented"
                        className="implemented-form-item">
                        {this.getFieldDecorator('implemented', {
                            rules: [{required: true, message: 'آیا ایده شما تا به حال اجرا شده است؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1"> اجرا شده</RadioButton>
                                <RadioButton value="0"> اجرا نشده</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Layer':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="وضعیت"
                        key="layer"
                        className="layer-form-item">
                        {this.getFieldDecorator('layer', {
                            rules: [{required: true, message: 'فایل بارگذاری شده لایه باز است یا تخت؟'}],
                        })(
                            <RadioGroup>
                                <RadioButton value="1">لایه باز</RadioButton>
                                <RadioButton value="0">تخت</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                );

            case 'Pages':
                return (
                    <FormItem
                        {...formItemLayout}
                        label="تعداد صفحات"
                        key="pages"
                        className="pages-form-item">
                        {this.getFieldDecorator('pages', {
                            rules: [{required: true, message: 'تعداد صفحات را وارد کنید!'}],
                        })(
                            <InputNumber min={1} max={1000}/>
                        )}
                    </FormItem>
                );

            case 'Size':
                return (
                    <div key="size"
                         className="size-form-item">

                        <FormItem
                            {...formItemLayout}
                            label="عرض"
                            key="width"
                            className="width-form-item">
                            {this.getFieldDecorator('width', {
                                rules: [{required: true, message: 'عرض را وارد کنید!'}],
                            })(
                                <InputNumber min={1}/>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="ارتفاع"
                            key="height"
                            className="height-form-item">
                            {this.getFieldDecorator('height', {
                                rules: [{required: true, message: 'ارتفاع را وارد کنید!'}],
                            })(
                                <InputNumber min={1}/>
                            )}
                        </FormItem>
                    </div>
                );

            case 'Video':
                return (
                    <FormItem key='video'
                        {...formItemLayout}
                        label="فایل ویدیویی"
                        className="file-form-item">
                        {this.getFieldDecorator('video', {
                            valuePropName: 'video',
                            getValueFromEvent: this.normFile,
                            rules: [{required: true, message: 'ویدیوی موردنظر را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="video/*" listType="file" action={BASE_URL+UPLOAD_PATH} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                {_.size(this.getFieldValue('video')) === 0 ? <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button> : '' }
                            </Upload>
                        )}
                    </FormItem>
                );

            case 'Audio':
                return (
                    <FormItem key='audio'
                        {...formItemLayout}
                        label="فایل صوتی"
                        className="file-form-item">
                        {this.getFieldDecorator('audio', {
                            valuePropName: 'audio',
                            getValueFromEvent: this.normFile,
                            rules: [{required: true, message: 'صوت موردنظر را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="audio/*" listType="file" action={BASE_URL+UPLOAD_PATH} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                {_.size(this.getFieldValue('audio')) === 0 ? <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button> : '' }
                            </Upload>
                        )}
                    </FormItem>
                );

            default:
                return;
        }
    }

    getFormComponents() {

        let type = this.props.match.params.type;

        type = TYPES.find((el) => { return el.type === type });

        let fields = [];
        type.fields.forEach((el) => {
            fields.push(this.selectComponent(el));
        });

        return fields;
    }

    render() {
        if (this.props.success) {
            return <Redirect to={'/content/' + this.props.content.id + '/' + this.props.content.name}/>;
        }

        let message = "";
        if (this.props.hasError) {
            message = "<p>Sorry! There was an error sending the package.</p>";
        }

        const {getFieldDecorator, getFieldValue} = this.props.form;

        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 20 },
        };

        return (
            <Form className="has-background-form" onSubmit={this.handleSubmit}>
                <div className="form-item-wrapper">
                    <span dangerouslySetInnerHTML={{__html: message}}></span>

                    <FormItem
                        {...formItemLayout}
                        label="عنوان"
                        className="title-form-item">
                        {getFieldDecorator('title', {
                            rules: [{required: true, message: 'عنوان موردنظر را وارد کنید!'}],
                        })(
                            <Input placeholder="عنوان محتوا را وارد کنید."/>
                        )}
                    </FormItem>

                    {this.getFormComponents()}
                    <FormItem
                        {...formItemLayout}
                        label="تصویر"
                        className="image-form-item">
                        {getFieldDecorator('image', {
                            valuePropName: 'image',
                            getValueFromEvent: this.normFile,
                            getValueFromLoad: this.normFile,
                            rules: [{required: true, message: 'تصویر موردنظر را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="image/*" listType="picture" action={BASE_URL+UPLOAD_PATH} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                {_.size(getFieldValue('image')) === 0 ? <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button> : '' }
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="فایل"
                        className="file-form-item">
                        {getFieldDecorator('file', {
                            valuePropName: 'file',
                            getValueFromEvent: this.normFile,
                        })(
                            <Upload name="file" listType="file" action={BASE_URL+UPLOAD_PATH} headers={getTokenObject()} onRemove={this.remove.bind(this)}>
                                <Button>
                                    <Icon type="upload"/> بارگذاری
                                </Button>
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="دسته بندی"
                        key="categories"
                        className="categories-form-item">
                        {this.getFieldDecorator('categories', {
                            rules: [{required: true, message: 'دسته‌بندی محتوا را انتخاب کنید.'}],
                        })(
                            <Select style={{width: 120}}>
                                {
                                    this.props.categories.map(function (el, i) {
                                        return <Option value={el.value} key={i}>{el.label}</Option>
                                    })
                                }
                            </Select>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="وب سایت ها"
                        className="websites-form-item">
                        {getFieldDecorator('websites', {
                            rules: [{required: true, message: 'لطفا وبسایت های مورد نظرتان را انتخاب کنید!'}],
                        })(
                            <CheckboxGroup options={this.props.websites} key="websites"/>
                        )}
                    </FormItem>

                   <FormItem
                        {...formItemLayout}
                        label="محل استفاده"
                        className="emplacements-form-item">
                        {getFieldDecorator('emplacements', {
                            rules: [{required: true, message: 'این محتوا در چه مکان هایی قابل استفاده است؟'}],
                        })(
                            <CheckboxGroup options={this.props.emplacements} key="emplacements"/>
                        )}
                    </FormItem>


                    <FormItem
                        {...formItemLayout}
                        label="متن"
                        className="description-form-item">
                        {getFieldDecorator('description', {
                            rules: [{required: true, message: 'متن محتوا را وارد کنید.'}],
                        })(
                            <TextArea placeholder="متن محتوا را وارد کنید." autosize={{ minRows: 4 }} />
                        )}
                    </FormItem>

                    <FormItem
                        wrapperCol={{ offset: 4}}
                        className="submit-form-item">
                        <Button type="primary" htmlType="submit" disabled={this.props.isLoading} loading={this.props.isLoading} >
                            تایید
                        </Button>
                    </FormItem>

                </div>
                {/*<Icon className="form-background" type="file-add" />*/}
            </Form>
        );
    }
}

function mapStateToProps(state) {
    return {
        hasError: state.createContentHasError,
        isLoading: state.createContentIsLoading,
        success: state.createContentSuccess,
        content: state.content,
        formats: state.formats,
        formatsHasError: state.formatsHasError,
        formatsIsLoading: state.formatsIsLoading,
        websites: state.websites,
        emplacements: state.emplacements,
        categories: state.categories,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        sendData: (data) => dispatch(createContent(data, 'GET')),
        fetchFormats: (url) => dispatch(formatsFetchData(url)),
        fetchWebsites: (url) => dispatch(websitesFetchData(url)),
        fetchEmplacements: (url) => dispatch(emplacementsFetchData(url)),
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        deleteFiles: (data) => dispatch(deleteFiles(data)),
    }
}

const ContentForm = Form.create()(Content);

export default connect(mapStateToProps, mapDispatchToProps)(ContentForm);