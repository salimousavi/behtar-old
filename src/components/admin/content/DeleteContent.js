import React, {Component} from 'react';
import {connect} from 'react-redux';
import {CONTENTS_PATH, BASE_URL} from '../../../tools/Constants';
import {contentsFetchData} from "../../../redux/actions/contents";
import { Icon, Popconfirm, message} from 'antd';
import {Header} from '../../../tools/Header';

class DeleteContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
        }
    }

    delete() {
        let url = BASE_URL + CONTENTS_PATH + '/' + this.props.content.id;
        this.setState({loading: true});

        fetch(url, Header({
            method: 'DELETE',
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                message.success('محتوا با موفقیت حذف شد.');
                this.props.fetchData(CONTENTS_PATH);
            })
            .catch((response) => {
                this.setState({error: true});
                message.error('خطا از سمت سرور');
            });

    }

    render() {
        return (
            <Popconfirm title="آیا از حذف این محتوا مطمئن هستید؟" onConfirm={this.delete.bind(this)} okText="بله"
                        cancelText="لغو">
                    <span className="action">
                        {this.state.error ? <Icon type="frown-o"/>:
                            this.state.loading ?
                                <Icon type="loading"/>
                                : <Icon type="delete"/>}
                    </span>
            </Popconfirm>
        )
    }
}

const StateToProps = (state) => {
    return {
    };
};

const DispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(contentsFetchData(url)),
    };
};

export default connect(StateToProps, DispatchToProps)(DeleteContent);