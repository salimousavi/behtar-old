import React, {Component} from 'react';
import {connect} from 'react-redux';
import {COMMENTS_PATH} from '../../../tools/Constants';
import {setPageIsAdmin} from "../../../redux/actions/page";
import Loading from "../../Loading";
import Time from '../../Time'
import {commentsFetchData, changeStatusComments, deleteComments} from "../../../redux/actions/comments";
import {Table, Icon, Menu, Button, Dropdown} from 'antd';
import {Link} from 'react-router-dom';
import DeleteComment from './DeleteComment'
import _ from 'lodash'

class AdminComments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedRows: []
        };
    }

    componentDidMount() {
        this.props.setPageIsAdmin(true);
        this.props.fetchData(COMMENTS_PATH);
    }

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    trim(inputString, maxLength) {

        let trimmedString = inputString.substr(0, maxLength);

        trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));

        return trimmedString;
    }

    publish() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.publish(ids);
    }

    unpublish() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.unpublish(ids);
    }

    delete() {
        let ids = this.state.selectedRows.map((r) => { return r.id});
        this.props.delete(ids);
    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the comments</p>;
        }

        if (this.props.isLoading) {
            return <Loading/>;
        }

        const dataSource = this.props.comments;

        const columns = [{
            title: 'نویسنده',
            dataIndex: 'username',
            key: 'username',
            render: (text, record) => <Link to={"/comment/" + record.id + "/" + text}>{text}</Link>,
        }, {
            title: 'محتوا',
            dataIndex: 'content_name',
            key: 'content_name',
            render: (text, record) => <Link to={"/content/" + record.content_id + "/" + text}>{text}</Link>,
        }, {
            title: 'متن',
            dataIndex: 'body',
            key: 'body',
        }, {
            title: 'تاریخ',
            dataIndex: 'time',
            key: 'time',
            render: (text) => <Time time={text}/>
            ,
        }, {
            title: 'وضعیت',
            dataIndex: 'status',
            key: 'status',
            render: (status) => <span>{status === '1' ? 'منتشرشده' : 'منتشر‌نشده'}</span>
            ,
        }, {
            title: ' ',
            key: 'action',
            render: (text, record) => (
                <div className="actions">
                    <Link to={"/comment/" + record.id + "/edit"} className="action"><Icon type="edit"/></Link>
                    <DeleteComment comment={record}/>
                </div>
            ),
        }];

        const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                this.setState({
                    selectedRows
                })
            },
        };

        return (
            <div className="admin-comments">
                <div className="title">
                    لیست پیام‌ها
                </div>
                <Table rowSelection={rowSelection} dataSource={dataSource} columns={columns}/>
                {_.isEmpty(this.state.selectedRows) ? '' : <Dropdown overlay={
                <Menu>
                    <Menu.Item>
                        <span onClick={this.delete.bind(this)}>
                        حذف پیام‌های انتخابی
                        </span>
                    </Menu.Item>
                    <Menu.Item>
                        <span onClick={this.publish.bind(this)}>
                       انتشار پیام‌های انتخابی
                        </span>
                    </Menu.Item>
                    <Menu.Item>
                        <span onClick={this.unpublish.bind(this)}>
                        عدم انتشار پیام‌های انتخابی
                        </span>
                    </Menu.Item>
                </Menu>} placement="bottomCenter">
                <Button className="actions-button">عملیات</Button>
            </Dropdown>}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        comments: state.comments,
        hasError: state.commentsHasError,
        isLoading: state.commentsIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        fetchData: (url) => dispatch(commentsFetchData(url)),
        publish: (ids, type) => dispatch(changeStatusComments(ids, 'publish')),
        unpublish: (ids, type) => dispatch(changeStatusComments(ids, 'unpublish')),
        delete: (ids) => dispatch(deleteComments(ids)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminComments);
