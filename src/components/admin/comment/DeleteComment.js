import React, {Component} from 'react';
import {connect} from 'react-redux';
import {COMMENTS_PATH, BASE_URL} from '../../../tools/Constants';
import {commentsFetchData} from "../../../redux/actions/comments";
import { Icon, Popconfirm, message} from 'antd';
import {Header} from '../../../tools/Header';

class DeleteComment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
        }
    }

    delete() {
        let url = BASE_URL + COMMENTS_PATH;
        this.setState({loading: true});

        fetch(url, Header({
            method: 'DELETE',
            body: JSON.stringify({ids:[this.props.comment.id]})
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                message.success('پیام با موفقیت حذف شد.');
                this.props.fetchData(COMMENTS_PATH);
            })
            .catch((response) => {
                this.setState({error: true});
                message.error('خطا از سمت سرور');
            });

    }

    render() {
        return (
            <Popconfirm title="آیا از حذف این پیام مطمئن هستید؟" onConfirm={this.delete.bind(this)} okText="بله"
                        cancelText="لغو">
                    <span className="action">
                        {this.state.error ? <Icon type="frown-o"/>:
                            this.state.loading ?
                                <Icon type="loading"/>
                                : <Icon type="delete"/>}
                    </span>
            </Popconfirm>
        )
    }
}

const StateToProps = (state) => {
    return {
    };
};

const DispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(commentsFetchData(url)),
    };
};

export default connect(StateToProps, DispatchToProps)(DeleteComment);