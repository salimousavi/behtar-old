import React, {Component} from 'react';
import '../../scss/admin/AdminDashboardBlock.scss'
import AdminDashboardBlock from "./AdminDashboardBlock";
import Cart from '../cart/Cart';

class AdminSidebar extends Component {

    render() {
        return (
            <div className="admin-sidebar wrapper">
                <Cart/>
                <AdminDashboardBlock/>
            </div>
        );
    }

}

export default AdminSidebar;