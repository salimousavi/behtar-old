import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Route} from 'react-router-dom';
import ContentForm from "./content/ContentForm";
import PackageForm from "./PackageForm";
import ContentEditForm from './content/ContentEditForm';
import AdminSidebarRight from "./AdminSidebar";
import AdminContents from './content/AdminContents';
import '../../scss/admin/AdminPage.scss'
import '../../scss/ant/AntTable.scss'
import AdminUsers from "./user/AdminUsers";
import AdminComments from "./comment/AdminComments";
import AdminSlides from "./slide/AdminSlides";
import SlideForm from "./slide/SlideForm";
import SlideEditForm from "./slide/SlideEditForm";

class AdminPage extends Component {
    render() {
        return (
            <div className="admin-page page-sections-wrapper">
                {this.props.pageIsAdmin ? <div className="sidebar sidebar-right page-section">
                        <AdminSidebarRight/>
                </div> : ''}
                <div className="main page-section">
                    <div className="wrapper">
                        <Route path="/admin/contents" component={AdminContents}/>
                        <Route path="/admin/users" component={AdminUsers}/>
                        <Route path="/admin/comments" component={AdminComments}/>
                        <Route exact path="/admin/slides/:slide_id/edit" component={SlideEditForm}/>
                        <Route exact path="/admin/slides/add" component={SlideForm}/>
                        <Route exact path="/admin/slides" component={AdminSlides}/>
                        <Route path="/add/:type" component={ContentForm}/>
                        <Route path="/content/:content_id/edit" component={ContentEditForm}/>

                        {this.props.isAdmin ? <Route path="/packageform" component={PackageForm}/> : ''}
                    </div>
                </div>

            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        isAdmin: state.isAdmin,
        pageIsAdmin: state.pageIsAdmin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminPage);
