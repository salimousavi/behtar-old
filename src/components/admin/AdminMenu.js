import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Menu, Icon} from 'antd';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import '../../scss/AdminBar.scss'
import {TYPES} from '../../tools/Constants';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class AdminMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            current: '',
        };
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(e) {
        this.setState({
            current: e.key,
        });
    }

    render() {

        if (!_.isEmpty(this.props.user) && this.props.isAdmin) {
            return (
                <div id="admin-menu" className="">
                    <div className="section">
                        <Menu
                            onClick={this.handleClick}
                            selectedKeys={[this.state.current]}
                            mode="horizontal"
                        >

                            <SubMenu title={
                                <span>
                    <Icon type="edit" />
                                    ایجاد محتوا

                    </span>}>
                                <MenuItemGroup>
                                    {
                                        TYPES.map((el, i) => {
                                            return (
                                                <Menu.Item key={"setting:" + i}>
                                                    <Link key={i} to={"/add/" + el.type}>{el.name}</Link>
                                                </Menu.Item>
                                            )})
                                    }
                                </MenuItemGroup>
                            </SubMenu>

                            <Menu.Item key="contents">
                                <Link to="/admin/contents">
                                    <Icon type="copy" />
                                    لیست محتوا
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="users">
                                <Link to="/admin/users">
                                    <Icon type="contacts" />
                                  کاربران
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="comments">
                                <Link to="/admin/comments">
                                    <Icon type="message" />
                                  پیام‌ها
                                </Link>
                            </Menu.Item>

                            <Menu.Item key="slides">
                                <Link to="/admin/slides">
                                    <Icon type="message" />
                                  اسلایدها
                                </Link>
                            </Menu.Item>
                        </Menu>
                    </div>
                </div>
            );
        }
        return (
            <div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        hasError: state.userHasError,
        isLoading: state.userIsLoading,
        isAdmin: state.isAdmin
    };
};


export default connect(mapStateToProps)(AdminMenu);