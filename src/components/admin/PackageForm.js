import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createPackage} from '../../redux/actions/package';
import {deleteFiles} from '../../redux/actions/content';
import {websitesFetchData} from '../../redux/actions/websites';
import {emplacementsFetchData} from '../../redux/actions/emplacements';
import { Redirect } from 'react-router-dom';
import {getTokenObject} from '../../tools/Header';
import {WEBSITES_PATH, EMPLACEMENTS_PATH, BASE_URL, UPLOAD_PATH} from '../../tools/Constants'
import {
    Form, InputNumber,
    Button, Upload, Icon, Input, Checkbox
} from 'antd';
import {setPageIsAdmin} from "../../redux/actions/page";
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const { TextArea } = Input;

class Package extends Component {

    componentWillUnmount() {
        this.props.setPageIsAdmin(false);
    }

    componentWillMount() {
        this.props.setPageIsAdmin(true);
    }

    componentDidMount() {
        this.props.fetchWebsites(WEBSITES_PATH);
        this.props.fetchEmplacements(EMPLACEMENTS_PATH);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let cart_ids = this.props.cart.map((el) => {
                    return el.id
                });
                values.cart = cart_ids;
                values.image = values.upload[0].response;

                let result = Object.assign({}, values);
                delete result.upload;
                console.log('Received values of form: ', values, result);
                this.props.create(result);
            }
        });
    };

    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    remove(e) {
        this.props.deleteFiles([e.response])
    }

    render() {

        if (this.props.success) {
            return <Redirect to={'/package/' + this.props.package.id} />;
        }

        const {getFieldDecorator} = this.props.form;
        const formItemLayout = {
            labelCol: {span: 4},
            wrapperCol: {span: 20},
        };

        return (

            <Form className="has-background-form" onSubmit={this.handleSubmit}>
                <div className="form-item-wrapper">

                    <FormItem
                        {...formItemLayout}
                        label="عنوان"
                    >
                        {getFieldDecorator('title', {
                            rules: [{required: true, message: "عنوان بسته را وارد نمایید."}],
                        })(
                            <Input placeholder="عنوان بسته را وارد نمایید."/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="شماره بسته"
                    >
                        {getFieldDecorator('number', {
                            rules: [{required: true, message: "شماره بسته را وارد نمایید."}],
                        })(
                            <InputNumber min={1} max={1000}/>
                        )}
                    </FormItem>


                    <FormItem
                        {...formItemLayout}
                        label="تصویر"
                    >
                        {getFieldDecorator('upload', {
                            valuePropName: 'file',
                            getValueFromEvent: this.normFile,
                            rules: [{required: true, message: 'تصویر بسته را بارگذاری کنید!'}],
                        })(
                            <Upload name="file" accept="image/*" listType="picture" action={BASE_URL+UPLOAD_PATH} headers={getTokenObject()}  onRemove={this.remove.bind(this)}>
                                <Button>
                                    <Icon type="upload"/>بارگذاری
                                </Button>
                            </Upload>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="وب سایت ها"
                    >
                        {getFieldDecorator('websites', {
                            rules: [{required: true, message: 'لطفا وبسایت های مورد نظرتان را انتخاب کنید!'}],
                        })(
                            <CheckboxGroup options={this.props.websites} key="websites"/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="محل استفاده"
                    >
                        {getFieldDecorator('emplacements', {
                            rules: [{required: true, message: 'این محتوا در چه مکان هایی قابل استفاده است؟'}],
                        })(
                            <CheckboxGroup options={this.props.emplacements} key="emplacements"/>
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="متن"
                    >
                        {getFieldDecorator('description', {
                            rules: [{required: true, message: 'متن محتوا را وارد کنید.'}],
                        })(
                            <TextArea placeholder="متن محتوا را وارد کنید." autosize={{ minRows: 4 }} />
                        )}
                    </FormItem>

                    <FormItem
                        wrapperCol={{ offset: 4}}
                    >
                        <Button type="primary" htmlType="submit"  disabled={this.props.isLoading} loading={this.props.isLoading} >
                            تایید
                        </Button>
                    </FormItem>
                </div>
                <Icon className="form-background" type="folder-add" />
            </Form>
        );
    }
}

const PackageForm = Form.create()(Package);


const mapStateToProps = (state) => {
    return {
        cart: state.cart,
        hasError: state.createPackageHasError,
        isLoading: state.createPackageIsLoading,
        success: state.createPackageSuccess,
        websites: state.websites,
        emplacements: state.emplacements,
        package: state._package,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        create: (data) => dispatch(createPackage(data)),
        fetchWebsites: (url) => dispatch(websitesFetchData(url)),
        fetchEmplacements: (url) => dispatch(emplacementsFetchData(url)),
        setPageIsAdmin: (data) => dispatch(setPageIsAdmin(data)),
        deleteFiles: (data) => dispatch(deleteFiles(data)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(PackageForm);
