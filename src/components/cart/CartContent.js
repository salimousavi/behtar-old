import React, { Component } from 'react';
import { Link} from 'react-router-dom'
import '../../scss/CartContent.scss'

class CartContent extends Component {

    render() {

        return (
            <div id="cart-content">

                <Link to={`/content/${this.props.content.id}/${this.props.content.name}`}>
                <img src={this.props.content.image} alt={this.props.content.name} title={this.props.content.name} />
                </Link>

            </div>
        );
    }

}

export default CartContent;