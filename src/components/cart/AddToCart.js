import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addToCart, removeFromCart} from '../../redux/actions/cart';
import {ADD_TO_CART_PATH} from '../../tools/Constants';
import {Icon, Tooltip} from 'antd';
import '../../scss/ActionCart.scss'

class AddToCart extends Component {

    addToCart() {
        this.props.add(ADD_TO_CART_PATH, this.props.content.id);
    }

    removeFromCart() {
        this.props.remove(ADD_TO_CART_PATH, this.props.content.id);
    }

    isInCart() {
        return this.props.cart.findIndex((c) => {
            return c.id === this.props.content.id
        });
    }

    render() {

        if (!this.props.isLogin && !this.props.isAdmin) {
            return <div></div>
        }

        if (this.props.isLoading) {
            return (
                <div>
                    <Icon type="loading" style={{fontSize: 28, color: '#7a4f15'}}/>
                </div>
            )
        }

        if (this.isInCart() !== -1) {
            return (
                <div>
                    <div className="remove-from-cart" onClick={() => this.removeFromCart()}>
                        <Tooltip title="حذف از سبد بسته محتوایی">
                            <Icon type="minus-circle-o" style={{fontSize: 28, color: '#c4c4c4'}}/>
                        </Tooltip>
                    </div>
                </div>
            );
        }

        if (this.isInCart() === -1) {
            return (
                <div className="add-to-cart" onClick={() => this.addToCart()}>
                    <Tooltip title="افزودن به سبد بسته محتوایی">
                        <Icon type="plus-circle-o" style={{fontSize: 28, color: '#c4c4c4'}}/>
                    </Tooltip>
                </div>
            );
        }

        return <div></div>
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        cart: state.cart,
        hasError: state.addToCartHasError,
        isLoading: state.addToCartIsLoading,
        isLogin: state.isLogin,
        isAdmin: state.isAdmin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        add: (url, id) => dispatch(addToCart(url, id)),
        remove: (url, id) => dispatch(removeFromCart(url, id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddToCart);