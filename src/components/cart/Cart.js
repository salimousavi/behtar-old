import React, { Component } from 'react';
import { connect } from 'react-redux';
import {cartFetchData , removeFromCart} from '../../redux/actions/cart';
import CartContent from './CartContent';
import {CART_PATH , ADD_TO_CART_PATH} from '../../tools/Constants';
import _ from 'lodash';
import { Link} from 'react-router-dom'
import {Icon} from 'antd';
import '../../scss/Cart.scss'

class Cart extends Component {

    constructor(props) {
        super(props);
        this.props.fetchData(CART_PATH);
    }

    emptyCart() {
        this.props.remove(ADD_TO_CART_PATH, 'all');
    }
    isAdmin() {
        return this.props.user.roles.findIndex((el) => {return el === 'admin'});
    }

    action() {
        if (!_.isEmpty(this.props.cart)) {
            return (
                <div className="cart-operation" >
                    <Icon type="close" style={{fontSize: 20, color: '#7a4f15'}} onClick={() => this.emptyCart()} />
                    <Link to="/packageform">
                        <Icon type="check" style={{fontSize: 20, color: '#7a4f15'}}/>
                    </Link>
                </div>
            );
        }
    }

    render() {

        // if (this.props.hasError) {
        //     return <p>Sorry! There was an error loading the contents</p>;
        // }
        //
        // if (this.props.isLoading) {
        //     // return <p>Loading…</p>;
        //     return <p></p>;
        // }


        return (
            <div className="cart">
                <div className="section clearfix">
                    {this.action()}
                    <div className="body">
                        {this.props.cart.map((content) => (
                            <CartContent key={content.id} content={content} />
                        ))}
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        cart: state.cart,
        hasError: state.cartHasError,
        isLoading: state.cartIsLoading,
        aHasError: state.addToCartHasError,
        aIsLoading: state.addToCartIsLoading,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(cartFetchData(url)),
        remove: (url, id) => dispatch(removeFromCart(url, id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);