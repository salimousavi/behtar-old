import React, {Component} from 'react';
import '../../scss/blocks/TeaserModeBlock.scss';
import {BASE_URL, CONTENTS_PATH} from '../../tools/Constants'
import {Header} from '../../tools/Header'
import Content from '../content/Content';

class PopularDesignsBlock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            contents: []
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        let url = BASE_URL + CONTENTS_PATH;
        this.setState({loading: true});
        let data = {
            method: 'POST',
            body: JSON.stringify({
                page: 1,
                number: 4,
                sort: '-update'
            }),
        };

        fetch(url, Header(data))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                this.setState({contents: p});
            })
            .catch((response) => {
                this.setState({error: true});
            });
    }

    render() {

        return (
            <div className="popular-designs-block block teaser-mode-block">
                <div className="block-title">
پربازدیدترین طرح‌ها
                </div>
                <div className="items row">
                    {this.state.contents.map((content, index)=> (
                        <Content display={1} key={index} content={content}/>
                    ))}
                </div>
            </div>
        );
    }

}

export default PopularDesignsBlock;