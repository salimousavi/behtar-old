import React, {Component} from 'react';
import { connect } from 'react-redux';
import {slidesFetchData} from '../../redux/actions/slides';
import $ from 'jquery';
import {Spin} from 'antd';
import _ from 'lodash';
import '../../scss/blocks/sliderblock/SliderBlock.scss';
window.$ = window.jQuery = $;
require('flexslider');

class SliderBlock extends Component {

    constructor(props) {
        super(props);
        this.props.fetchData();
        console.log('constructor is fired');
    }

    componentDidMount(){
        $(window).load(function() {
            console.log('win loaded');
            $('.slider .position-relative').flexslider({
                animation: "slide"
            });
        });
    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.isLoading) {
            return <div className="loading"> <Spin/></div>;
        }

        if (!_.isEmpty(this.props.slides)) {
            $('.slider .position-relative').flexslider({
                animation: "slide"
            });
            return (
                <div className="slider-block block">
                    <div className="title">
                        راز بندگی از نگاه امام رضا (ع)
                    </div>
                    <div className="slider" id="slider">
                        <div className="flexslider position-relative">
                            <ul className="slides">
                                {this.props.slides.map((slide, i)=>(
                                    <li key={i} className="slide">
                                        <a href={slide.link}>
                                            <img src={slide.image} />
                                            <div className="flex-caption">
                                                <div className="headline">
                                                    {slide.headline}
                                                </div>
                                                <div className="title">
                                                    {slide.title}
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
            );
        }

        return <div className="loading"> <Spin/></div>;
    }

}


const mapStateToProps = (state) => {
    return {
        slides: state.slides,
        hasError: state.slidesHasError,
        isLoading: state.slidesIsLoading,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(slidesFetchData()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SliderBlock);