import React, {Component} from 'react';
import AudioPlayer from 'react-responsive-audio-player';
import {BASE_URL, CONTENTS_PATH} from '../../tools/Constants';
import {Header} from '../../tools/Header';
import {Link} from "react-router-dom";

import '../../scss/blocks/LastAudiosBlock.scss';

class LastAudiosBlock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            contents: []
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        let url = BASE_URL + CONTENTS_PATH + '?types=6';
        this.setState({loading: true});
        let data = {
            method: 'POST',
            body: JSON.stringify({
                page: 1,
                number: 4,
                sort: '-create'
            }),
        };

        fetch(url, Header(data))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                this.setState({contents: p});
            })
            .catch((response) => {
                this.setState({error: true});
            });
    }

    render() {
        return (
            <div className="last-audios-block">
                <div className="title">
                    جدیدترین صوت‌ها
                </div>
                <div className="audios">
                    {this.state.contents.map((content, index)=> (
                        <div className="audio-wrapper" key={index}>
                            {/*<Link to={ "/content/" + content.id + '/' + content.name }>*/}
                            <AudioPlayer playlist={[{url: content.audio[0].url, displayText: content.name}]} hideBackSkip={true} hideForwardSkip={true} disableSeek={true}/>
                            {/*</Link>*/}
                        </div>
                    ))}
                </div>
            </div>
        );
    }

}

export default LastAudiosBlock;