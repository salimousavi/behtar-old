import React, {Component} from 'react';
import Video from "react-h5-video";
import {BASE_URL, CONTENTS_PATH} from '../../tools/Constants';
import {Header} from '../../tools/Header';
import '../../scss/blocks/LastVideoBlock.scss';
class LastVideoBlock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            contents: []
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        let url = BASE_URL + CONTENTS_PATH + '?types=7';
        this.setState({loading: true});
        let data = {
            method: 'POST',
            body: JSON.stringify({
                page: 1,
                number: 1,
                sort: '-create'
            }),
        };

        fetch(url, Header(data))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                this.setState({contents: p});
            })
            .catch((response) => {
                this.setState({error: true});
            });
    }

    render() {
        return (
            <div className="last-video-block">
                <div className="title">
                    جدیدترین فیلم‌ها
                </div>
                <div className="player">
                    <div className="video-wrapper">
                        {this.state.contents.map((el, i) => {
                            return (
                                <Video
                                    key={i}
                                    sources={[el.video[0].url]}
                                    poster={el.video_poster}
                                    width="100%"
                                    height="auto"
                                    // controlPanelStyle="fixed"
                                >
                                </Video>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }

}

export default LastVideoBlock;