import React, {Component} from 'react';
import Link from "./Link";


class LinksWrapper extends Component {

    render() {
        let LINKSWRAPPER = this.props.data;
        // console.log(this.props.data);
        let iconStyle = {
            backgroundColor: LINKSWRAPPER.COLOR
        };

        return (
            <div className="links-wrapper">
                <div className="title">
                    <i style={iconStyle}>
                    </i>
                    { LINKSWRAPPER.TITLE }
                </div>
                <div className="links">

                    {LINKSWRAPPER.LINKS.map((link, index) => (
                        <Link key={index} data={link} />
                    ))}

                </div>
            </div>
        );
    }

}

export default LinksWrapper;