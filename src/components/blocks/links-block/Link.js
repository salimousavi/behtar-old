import React, {Component} from 'react';


class Link extends Component {

    render() {
        let LINK = this.props.data;
        return (
            <div className="link-item">
                <a href={ "http://" + LINK.LINK }>
                    <div className="title">
                        {LINK.TITLE}
                    </div>
                    <div className="link">
                        {LINK.LINK}
                        </div>
                </a>
            </div>
        );
    }

}

export default Link;
