import React, {Component} from 'react';
import {LINKS} from './_links'
import '../../../scss/blocks/LinksBlock.scss';
import LinksWrapper from "./LinksWrapper";


class LinksBlock extends Component {

    render() {

        // console.log(LINKS);
        // console.log(LINKS['LINKS_BLOCK']);
        return (
            <div className="links-block block">
                <div className="links-container">

                    {LINKS.LINKS_BLOCK.map((linksWrapper, index) => (
                        <LinksWrapper key={index} data={linksWrapper} />
                    ))}

                </div>
            </div>
        );
    }

}

export default LinksBlock;