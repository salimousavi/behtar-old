const LINKS = {
    LINKS_BLOCK: [
        {
            TITLE: 'نگاه بهتر',
            COLOR: '#7bead9',
            LINKS: [
                {
                    TITLE: 'کتاب خدا',
                    LINK: 'Quran.behtar.ir'
                },
                {
                    TITLE: 'ولی خدا',
                    LINK: 'Ahlebeyt.behtar.ir'
                },
                {
                    TITLE: 'حکم خدا',
                    LINK: 'Ahkam.behtar.ir'
                },
            ]
        },
        {
            TITLE: 'زندگی بهتر',
            COLOR: '#7bccea',
            LINKS: [
                {
                    TITLE: 'مسجد بهتر',
                    LINK: 'Masjed.behtar.ir'
                },
                {
                    TITLE: 'مدرسه بهتر',
                    LINK: 'Madrese.behtar.ir'
                },
                {
                    TITLE: 'خانواده بهتر',
                    LINK: 'Khanevade.behtar.ir'
                },
                {
                    TITLE: 'هیئت بهتر',
                    LINK: 'Heyat.behtar.ir'
                },
            ]
        },
        {
            TITLE: 'رسانه بهتر',
            COLOR: '#ffbb78',
            LINKS: [
                {
                    TITLE: 'کتاب بهتر',
                    LINK: 'Ketab.behtar.ir'
                },
                {
                    TITLE: 'بازی بهتر',
                    LINK: 'Bazi.behtar.ir'
                },
                {
                    TITLE: 'فیلم بهتر',
                    LINK: 'Film.behtar.ir'
                },
                {
                    TITLE: 'نت بهتر',
                    LINK: 'Net.behtar.ir'
                },
                {
                    TITLE: 'ایده بهتر',
                    LINK: 'Idea.behtar.ir'
                },
            ]
        },
        {
            TITLE: 'جامعه بهتر',
            COLOR: '#e7a1d7',
            LINKS: [
                {
                    TITLE: 'اقتصاد بهتر',
                    LINK: 'Eghtesad.behtar.ir'
                },
                {
                    TITLE: 'سیاست بهتر',
                    LINK: 'Siasat.behtar.ir'
                },
                {
                    TITLE: 'طب بهتر',
                    LINK: 'Tebb.behtar.ir'
                },
            ]
        },
    ]
};


module.exports = {
    LINKS
};