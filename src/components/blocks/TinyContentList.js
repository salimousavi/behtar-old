import React, {Component} from 'react';
import {getTypeName} from '../../tools/Taxonomies';
import {Link} from "react-router-dom";

class TinyContentList extends Component {

    render() {
        return (
            <div className="item">
                <Link to={ "/content/" + this.props.content.id + '/' + this.props.content.name }>
                    <sapn className="type">
                        {getTypeName(this.props.content.type)}
                        <span>|</span>
                    </sapn>
                    <span className="title">
                    {this.props.content.name}
                </span>
                </Link>
            </div>

        );
    }

}

export default TinyContentList;