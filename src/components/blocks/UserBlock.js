import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {Form, Icon, Input, Button} from 'antd';
import {logout, userFetchData} from '../../redux/actions/user';
import {USER_PATH} from '../../tools/Constants';
import '../../scss/blocks/UserBlock.scss'
import {message} from 'antd';

const FormItem = Form.Item;

class UserBlock extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.login(USER_PATH, values);
            }
        });

    };

    render() {
        if (this.props.loginError) {
            message.config({
                top: 50,
                duration: 4,
            });
            message.error('اطلاعات ورودی صحیح نمی باشد.');
        }

        if (this.props.isLogin) {
            return (
                <div className="user-block block">
                    <div className="inner logged-in">
                        <div className="text">
                            <b>
                                سلام {this.props.user.name} عزیز!
                            </b>
                            <div>
                                ما در مجموعه فرهنگ بهتر تلاش می‌کنیم تا پیوند زیبایی میان طرح و محتوا ایجاد کنیم.
                                <br/>
                                با پیشنهادات و کمک‌های خودت به ما و خانواده بزرگ فرهنگ بهتر کمک کن.
                                <br/>
                                ممکنه ایده‌ای که تو ذهن شماست راه حل مشکل بزرگ یه نفردیگه باشه.
                                پس در تولید محتوای سایت مشارکت کن.
                            </div>

                        </div>
                        <div className="menu">
                            <div className="flex-wrapper">
                                <div className="flex flex-1">
                                    <Link to="/" className="link">ارسال طرح</Link>
                                </div>
                                <div className="flex flex-1">
                                    <Link to="/" className="link">ارسال ایده</Link>
                                </div>
                            </div>
                            <Link to="/" className="link">صفحه کاربری</Link>

                            <div className="link" onClick={this.props.logout.bind(this)} >خروج</div>
                        </div>
                    </div>
                </div>
            )
        }

        const {getFieldDecorator} = this.props.form;
        return (
            <div className="user-block block">
                <div className="inner not-logged-in">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem>
                            {getFieldDecorator('user', {
                                rules: [{required: true, message: 'نام کاربری یا ایمیل خود را وارد نمایید.'}],
                            })(
                                <Input prefix={<Icon type="user"/>} placeholder="نام کاربری یا ایمیل"/>
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('pass', {
                                rules: [{required: true, message: 'رمز عبور خود را وارد نمایید.'}],
                            })(
                                <Input prefix={<Icon type="lock"/>} type="password" placeholder="رمز عبور"/>
                            )}
                        </FormItem>
                        <FormItem>
                            <Button type="primary" htmlType="submit" className="login-form-button" loading={this.props.isLoading} >
                                ورود
                            </Button>
                            <div className="links">
                                <Link to="/register">ثبت نام</Link>
                                <Link to="/">رمزم را فراموش کرده‌ام</Link>
                            </div>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }
}

const LoginForm = Form.create()(UserBlock);

function mapStateToProps(state) {
    return {
        isLoading: state.userIsLoading,
        isLogin: state.isLogin,
        loginError: state.loginError,
        user: state.user,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        login: (url, form) => dispatch(userFetchData(url, form)),
        logout: () => dispatch(logout()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);