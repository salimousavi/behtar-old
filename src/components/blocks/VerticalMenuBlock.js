import React, {Component} from 'react';
import '../../scss/blocks/VerticalMenuBlock.scss';
import link1 from '../../images/img/menu/1.png';
import link2 from '../../images/img/menu/2.png';
import link3 from '../../images/img/menu/3.png';
import link4 from '../../images/img/menu/4.png';

class VerticalMenuBlock extends Component {

    render() {
        return (
            <div className="vertical-menu-block block">
                <div className="title">
            بسته های هفتگی
                </div>
                <div className="links">
                    <div className="link">
                        <img src={link1} alt="" />
                    </div>
                    <div className="link">
                        <img src={link2} alt="" />
                    </div>
                    <div className="link">
                        <img src={link3} alt="" />
                    </div>
                    <div className="link">
                        <img src={link4} alt="" />
                    </div>
                </div>
            </div>
        );
    }

}

export default VerticalMenuBlock;