import React, {Component} from 'react';
import '../../scss/blocks/PlaceBlock.scss';
import place1 from '../../images/icons/place/icon1.png';
import place2 from '../../images/icons/place/icon2.png';
import place3 from '../../images/icons/place/icon3.png';
import place4 from '../../images/icons/place/icon4.png';
import place5 from '../../images/icons/place/icon5.png';
import place6 from '../../images/icons/place/icon6.png';

class PlaceBlock extends Component {

    render() {
        return (
            <div className="place-block block">
                <div className="title">
                    برای کجا دنبال طرح میگردی؟
                </div>
                <div className="places">
                    <div className="place">
                        <img src={place1} alt="اداره" />
                    </div>
                    <div className="place">
                        <img src={place2} alt="مدرسه" />
                    </div>
                    <div className="place">
                        <img src={place3} alt="مسجد" />
                    </div>
                    <div className="place">
                        <img src={place4} alt="" />
                    </div>
                    <div className="place">
                        <img src={place5} alt="" />
                    </div>
                    <div className="place">
                        <img src={place6} alt="آپارتمان" />
                    </div>
                </div>
            </div>
        );
    }

}

export default PlaceBlock;