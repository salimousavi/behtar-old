import React, {Component} from 'react';
import '../scss/Loading.scss'

class Loading extends Component {

    render() {
        return (

            <div className="row">
                <div className="clearfix">
                    <div className="loading">
                        <ul className="bokeh">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                </div>

            </div>
        );
    }
}

export default Loading;
