import React, {Component} from 'react';
import {connect} from 'react-redux';
import MainSection from "./MainSection";
import SidebarRight from './SidebarRight';
import SidebarLeft from './SidebarLeft';

class NormalPage extends Component {

    render() {
        if (this.props.pageIsAdmin) {
            return <div></div>
        }

        return (
            <div className="page-sections-wrapper">

                <div className="sidebar sidebar-right page-section">
                    <SidebarRight/>
                </div>
                <div className="main page-section">
                    <MainSection/>
                </div>

                <div className="sidebar sidebar-left page-section">
                    <SidebarLeft/>
                </div>
            </div>
        );


    }
}


const mapStateToProps = (state) => {
    return {
        isAdmin: state.isAdmin,
        pageIsAdmin: state.pageIsAdmin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NormalPage);