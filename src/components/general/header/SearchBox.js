import React, { Component } from 'react';
import {Icon} from 'antd'
import '../../../scss/SearchBox.scss';

class SearchBox extends Component {

    render() {
        return (
            <form action="" className="search-box">
                <input type="text" className="search-string" placeholder="واژه ای را جستجو کنید..."/>
                <button className="search-button">
                    <Icon type="search" />
                </button>
            </form>
        );
    }

}

export default SearchBox;