import React, {Component} from 'react';
import {connect} from 'react-redux';
import {logout, isAdmin} from '../../../redux/actions/user';
import _ from 'lodash';
import {Link} from 'react-router-dom';
import LoginForm from '../../blocks/UserBlock';
import { Popover } from 'antd';
class UserMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            form: false,
        }
    }

    setFormShow(state) {
        this.setState({form:state})
    }

    state = {
        visible: false,
    }
    hide = () => {
        this.setState({
            visible: false,
        });
    }
    handleVisibleChange = (visible) => {
        this.setState({ visible });
    }

    render() {

        if (!_.isEmpty(this.props.user)) {
            return (
                <div id="user-menu" className="menu-wrapper">
                <div className="section">
                    <ul className="links list-inline clearfix">
                        <li className="last"><a href="/user/login" title="">{this.props.user.name}</a></li>
                        <li onClick={() => this.props.logout()} className="last">خروج</li>
                    </ul>
                </div>
            </div>
            );
        }
        return (
            <div id="user-menu" className="menu-wrapper">
                <div className="section">
                    <ul className="links list-inline clearfix">
                        <li className="first"><Link to="/register" >ثبت نام</Link></li>
                        {/*<li className="last" onClick={() => {this.setState({form:!this.state.form})}}  >ورود</li>*/}
                        <li className="last" >
                            <Popover
                                content={<LoginForm onVisibleChange={this.handleVisibleChange.bind(this)} />}
                                placement="bottomLeft"
                                title="فرم ورود"
                                trigger="click"
                                visible={this.state.visible}
                                onVisibleChange={this.handleVisibleChange}
                            >
                                ورود
                            </Popover>
                        </li>
                    </ul>
                </div>
                {this.state.form ? <LoginForm  setFormShow={this.setFormShow.bind(this)} /> : '' }
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        hasError: state.userHasError,
        isLoading: state.userIsLoading,
        success: state.userLoginSuccess,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(logout());
            dispatch(isAdmin(false))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserMenu);