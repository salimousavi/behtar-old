import React, { Component } from 'react';

class UserMenu extends Component {

    render() {
        return (
            <div id="contact-menu" className="menu-wrapper"><div className="section">
                <ul className="links clearfix">
                    <li className="first active"><a href="/" >تماس با ما</a></li>
                    <li className=""><a href="/" >درباره ما</a></li>
                    <li className="last"><a href="/">دوستان</a></li>
                </ul>
            </div></div>
        );
    }

}

export default UserMenu;