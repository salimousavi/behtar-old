import React, { Component } from 'react';
import ContactMenu from './ContactMenu'
import UserMenu from './UserMenu'
import MainMenu from './MainMenu'
import AdminMenu from '../../admin/AdminMenu'
import HeaderBackground from '../../../images/header-background.png';
import '../../../scss/Header.scss'


class Header extends Component {

    render() {
        return (
            <div id="header"><div className="section clearfix">

                <a href="/" title="خانه" id="logo">
                    <img src={HeaderBackground} alt="خانه" />
                </a>

                <AdminMenu/>


                <div className="menu-wrappers color-white">

                    <UserMenu />
                    <ContactMenu />
                </div>

                <div className="region region-header">
                    <section id="main-menu-wrapper" className="block block-masjed clearfix">


                        <MainMenu/>

                    </section>
                </div>
            </div></div>
        );
    }

}

export default Header;