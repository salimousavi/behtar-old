import React, { Component } from 'react';
import MainMenu from './MainMenu'
import AdminMenu from '../../admin/AdminMenu'
// import logo from '../../../images/logo.png';
import logo from '../../../images/logo-kh.png';
import Telegram from '../../../images/icons/telegram.png';
import '../../../scss/Header.scss'
import SearchBox from "./SearchBox";
import {Link} from 'react-router-dom';


class Header extends Component {

    render() {
        return (
            <div id="header"><div className="section container clearfix">
                <AdminMenu/>
                <div className="header-inner">

                    <div className="logo-wrapper">
                        <div className="wrapper">
                            <Link to="/" title="خانه" id="logo">
                                <img src={logo} alt="خانه" />
                            </Link>
                        </div>
                    </div>

                    <div className="menu-wrapper">
                        <div className="wrapper">
                            <MainMenu/>
                            <div className="telegram">
                                <a href="">
                                    کانال ها
                                    <img src={Telegram} alt="telegram" />
                                </a>
                            </div>
                        </div>
                    </div>

                    <div className="search-box-wrapper">
                        <div className="wrapper">
                            <SearchBox/>
                        </div>
                    </div>
                </div>

            </div></div>
        );
    }

}

export default Header;