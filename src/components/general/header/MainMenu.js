import React, {Component} from 'react';
import {Link} from 'react-router-dom';

// import Logo from '../../../images/masjed-logo.png';

class MainMenu extends Component {

    render() {
        return (
            <div id="main-menu">
                <ul>
                    {/*<div className='active item'><Link to='/'>*/}
                    {/*<span className='text'>مسجد بهتر</span>*/}
                    {/*/!*<img src={Logo} alt="logo" />*!/*/}
                    {/*</Link></div>*/}
                    <div className="main-menu-wrapper">
                        <div className="menu-bar">
                            <div id="nav-icon3">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                        <div className="items">
                            <div className="item">
                                <Link to="/literature">
                                    متن نوشته
                                </Link>
                            </div>
                            <div className="item">
                                <Link to="/journal">
                                    نشریه
                                </Link>
                            </div>
                            <div className="item">
                                <Link to="/poster">
                                    پوستر
                                </Link>
                            </div>
                            <div className="item">
                                <Link to="/leaflet">
                                    جزوه‌و‌بروشور
                                </Link>
                            </div>
                            <div className="item">
                                <Link to="/audios">
                                صوت
                                </Link>
                            </div>
                            <div className="item">
                                <Link to="/videos">
                                    فیلم
                                </Link>
                            </div>
                            <div className="item">
                                <Link to="/ideas">
                                    ایده
                                </Link>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>
        );
    }

}

export default MainMenu;