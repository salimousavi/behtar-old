import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../../scss/Breadcrumb.scss';

class Breadcrumb extends Component {

    get_path_list() {

        if (this.props.path === "/") {
            return [];
        }

        let list = this.props.path.split('/');
        return list;
    }

    get_link() {
        let list = this.get_path_list();
        let obs = [];

        list.forEach(function (e) {

            let name = Breadcrumb.get_ob(e, list);

            if (name) {
                obs.push({
                    name: name,
                    path: e,
                })
            }
        });

        return obs;
    }

    static get_ob(item, list) {
        switch (item) {
            case "":
                return 'صفحه اصلی';

            case "board":
                return 'تابلو اعلانات';

            case "packages":
                return 'بسته‌ها';

            case "posters":
                return 'پوسترها';

            case "literature":
                return 'متن نوشته';

            case "ideas":
                return 'ایده';

            case "journal":
                return 'نشریه';

            case "leaflet":
                return 'بروشور و جزوه';

            case "audios":
                return 'صوت';

            case "videos":
                return 'فیلم';

            case "graphics":
                return 'طرح‌های مناسبتی';

            case "content":
                return list[list.length - 1];

            case "package":
                return list[list.length - 1];

            default:
                return null;
        }
    }

    render() {
        let l = "";
        return (
            <div className="Breadcrumb">
                {
                    this.get_link().map((element, index) => {
                        if (element.path) {
                            l = l + "/" + element.path;
                        }
                        else {
                            return <Link key={index} to="/"> { element.name } </Link>
                        }
                        return <Link key={index} to={l}> { element.name } </Link>
                    })}
            </div>
        );
    }

}

export default Breadcrumb;