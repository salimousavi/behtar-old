import React, { Component } from 'react';

class AboutUs extends Component {

    render() {
        return (
            <div className="about-us">
                <div className="title">
درباره ما:
                </div>
                <div className="deatil">
                    گروه «فرهنگ بهتر» از سال 1390 با هدف تعالی
                    فرهنگ ایرانی اسلامی، در سه حوزه «نگاه بهتر»، «زندگی بهتر» و «رسانه بهتر»
                    کار خود را آغاز و درگاه اینترنتی خود را از سال 1390 راه اندازی کرده است.
                    <span className="more">
                        ادامه مطلب
                    </span>
                </div>
            </div>
        );
    }

}

export default AboutUs;