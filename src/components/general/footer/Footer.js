import React, {Component} from 'react';
import AboutUs from "./AboutUs";
import ContactUs from "./ContactUs";
import '../../../scss/Footer.scss'
import rainbow from '../../../images/rainbow.png'
import rss from '../../../images/icons/rss.png'
import logo from '../../../images/logo2.png'

class Footer extends Component {

    render() {
        return (
            <footer className="footer clearfix">
                <div className="first-row">
                    <div className="container">
                        <div className="sections-wrapper">
                            <div className="section section1">
                                <AboutUs/>
                                <ContactUs/>
                            </div>
                            <div className="section section2">
                                <img src={logo} alt="behtar" className="logo"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="last-row">
                    <div className="container">
                        <div className="sections-wrapper">
                            <div className="section section1">
                                <div className="rainbow">
                                    {/*<img src={rainbow}/>*/}
                                </div>
                                <div className="copyright">
                                    کلیه حقوق مادی و معنوی این سایت متعلق به کانون فرهنگی تبلیغی فرهنگ بهتر است
                                </div>
                            </div>
                            <div className="section section2">
                                <img src={rss} alt="RSS"/>
                                <span>
                                RSS
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }

}

export default Footer;