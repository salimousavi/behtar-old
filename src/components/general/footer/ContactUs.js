import React, { Component } from 'react';
import map from '../../../images/map.png'

class ContactUs extends Component {

    render() {
        return (
            <div className="contact-us">
                <div className="contact-us-inner">
                    <div className="map">
                        <img src={map} alt="" className="logo"/>

                    </div>
                    <div className="text">
                        <div className="title">
                            تماس باما:
                        </div>
                        <div className="detail">

                            رایانامه:
                            <span className="en"> info@farhangebahtar.ir </span>
                            <br/>
                            سامانه پیامکی:
                            <span> 100150160170 </span>
                            <br/>
                            کانال تلگرام:
                            <a href="https://t.me/farhangebehtar_ir" className="en" target="_blank"> farhangebehtar_ir@ </a>
                            <br/>
                            نشانی:
                            <span> تهران، تقاطع رسالت-باقری، خیابان شهید صادقی، پلاک 112، مجتمع شهید اسداللهی </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default ContactUs;