import React, { Component } from 'react';
import '../../../scss/Comment.scss'

class Comment extends Component {

    render() {
        return (
                <div className="Comment" id={"comment_" + this.props.comment.id}><div className="section clearfix">

                    <div className="header">

                        <div className="name-wrapper">
                            {this.props.comment.subject}
                        </div>


                        <div className="date-wrapper">
                            25 اسفند 95
                        </div>

                    </div>
                    <div className="body clearfix">

                        <div className="detail-wrapper">

                            <div className="title-wrapper">
                                {this.props.comment.username} :
                            </div>

                            <div className="text-wrapper">

                                <span dangerouslySetInnerHTML={{__html: this.props.comment.body}} />

                            </div>

                        </div>

                    </div>

                </div></div>

        );
    }

}

export default Comment;