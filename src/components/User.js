import React, {Component} from 'react';
import {connect} from 'react-redux';
import {userFetchData} from '../redux/actions/user';
import {USER_PATH} from '../tools/Constants';
class User extends Component {
    componentDidMount() {
        this.props.fetchData(USER_PATH);
    }

    render() {
        return (
            <div>
            </div>
        );


    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        hasError: state.userHasError,
        isLoading: state.userIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(userFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
