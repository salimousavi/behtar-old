import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../scss/Block.scss'


class Block extends Component {

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        return (
            <div className="block">

                <div className="header">
                    <h4>
                        {this.props.name}
                    </h4>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        content: state.content,
        hasError: state.contentHasError,
        isLoading: state.contentIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Block);
