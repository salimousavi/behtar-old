import React, {Component} from 'react';
import {connect} from 'react-redux';
import {packageFetchData, packageDownload} from '../../redux/actions/package'
import {PACKAGES_PATH} from '../../tools/Constants';
import {Icon} from 'antd';
import Content from '../content/Content';
import '../../scss/PackageFull.scss'

//   route:  /board/packages/:package_id

class PackageFull extends Component {

    constructor(props, context) {
        super(props);
        this.props.fetchData(PACKAGES_PATH + '/' + this.props.match.params.package_id);
    }

    downloadPackage(e) {
        let url = PACKAGES_PATH + '/' + this.props.package.id + '/download';
        this.props.download(url);
    }

    render() {
        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }
        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        return (
            <div className="package package-full">
                <div className="header">
                    {this.props.match.params.package_id}-
                    {this.props.package.name}
                    <div className="download-link" onClick={this.downloadPackage.bind(this)}>
                        <Icon type="download" />
                        <span>
                            دریافت یکجای بسته
                        </span>
                    </div>
                </div>
                <div className="body">
                    {this.props.package.contents.map((content) => (
                        <Content display={2} key={content.id} content={content}/>
                    ))}
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        package: state._package,
        hasError: state.packageHasError,
        isLoading: state.packageIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(packageFetchData(url)),
        download: (url) => dispatch(packageDownload(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PackageFull);