import React, { Component } from 'react';
import { connect } from 'react-redux';
import {packagesFetchData} from '../../redux/actions/packages';
import PackageTeaser from './PackageTeaser';
import {PACKAGES_PATH} from '../../tools/Constants';

import '../../scss/Packages.scss';

class Packages extends Component {

    constructor(props) {
        super(props);
        this.props.fetchData(PACKAGES_PATH);
    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        return (
            <div className="packages">
                <div className="section">
                    <div className="header">
                        از همین امروز تابلو اعلانات مسجد، مدرسه، اداره یا آپارتمان‌تون رو راه بندازید! محتوا و طراحی با ما، چاپ و نصب با شما. بسم الله
                    </div>
                    <div className="body">
                        <div className="row">
                            {this.props.packages.map((pack) => (
                                <PackageTeaser key={pack.id} package={pack} />
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        packages: state.packages,
        hasError: state.packagesHasError,
        isLoading: state.packagesIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(packagesFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Packages);