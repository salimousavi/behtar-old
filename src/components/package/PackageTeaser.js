import React, { Component } from 'react';
import { Link} from 'react-router-dom'
import '../../scss/PackageTeaser.scss'

class PackageTeaser extends Component {

    render() {
        // const COLORS = [
        //     '01cfe4',
        //     'ffb300',
        //     '3c97ff',
        //     '98c04d',
        // ];

        // let currentColor = '#' + COLORS[ (this.props.package.id - 1) % 4 ];

        return (
            <div id="package" className="col-sm-3 clearfix package package-teaser">

                <div className="box info-box">

                    <Link to={ "/package/" + this.props.package.id + "/" + this.props.package.name}>

                    <div className="info">
                        بسته {this.props.package.number}-
                        {this.props.package.name}
                    </div>

                    <div className="image">
                            <img src={this.props.package.image} alt={this.props.package.alt} />
                    </div>
                    </Link>

                    <div className="link">
                        <a href="/">
                            مشاهده و دریافت بسته های قبل
                        </a>
                    </div>
                </div>

                {/*<div className="rate-wrapper right rate-link">*/}
                    {/*<div className="circle" style={{backgroundColor: currentColor}}> </div>*/}
                    {/*<div className="box box-on-circle">*/}
                        {/*ایده ها*/}
                    {/*</div>*/}
                {/*</div>*/}

                {/*<div className="rate-wrapper left rate-info clearfix">*/}
                    {/*<div className="circle" style={{backgroundColor: currentColor}}> </div>*/}
                    {/*<div className="box box-on-circle">*/}
                        {/*تصویر نمونه های اجرا شده*/}
                    {/*</div>*/}
                {/*</div>*/}

            </div>
        );
    }

}

export default PackageTeaser;