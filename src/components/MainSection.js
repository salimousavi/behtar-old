import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import FullContent from './content/FullContent';
import Packages from './package/Packages';
import PackagesFull from './package/PackageFull';
import SliderBlock from './blocks/SliderBlock';
import RegistrationForm from './Register';
import {checkDisplay} from '../tools/BlockDisplay';
import LastProductsBlock from "./blocks/LastProductsBlock";
import PopularDesignsBlock from "./blocks/PopularDesignsBlock";
import LastAudiosBlock from "./blocks/LastAudiosBlock";
import LastVideoBlock from "./blocks/LastVideoBlock";
import Contents from './content/Contents';

class MainSection extends Component {

    render() {
        return (
            <div className="MainSection wrapper">

                <Route exact path="/" component={SliderBlock}/>
                <Route exact path="/" component={LastProductsBlock}/>
                <Route exact path="/" component={PopularDesignsBlock}/>
                <div className="multimedia flex-wrapper">
                    <div className="flex flex-1">
                        <Route exact path="/" component={LastVideoBlock}/>
                    </div>
                    <div className="flex flex-1">
                        <Route exact path="/" component={LastAudiosBlock}/>
                    </div>
                </div>

                <Route path="/leaflet" render={() => <Contents type="leaflet"/>}/>
                <Route path="/graphics" render={() => <Contents type="graphics"/>}/>
                <Route path="/videos" render={() => <Contents type="videos"/>}/>
                <Route path="/audios" render={() => <Contents type="audios"/>}/>
                <Route path="/ideas" render={() => <Contents type="ideas"/>}/>
                <Route path="/journal" render={() => <Contents type="journal"/>}/>
                <Route path="/literature" render={() => <Contents type="literature"/>}/>
                <Route path="/poster" render={() => <Contents type="poster"/>}/>
                <Route path="/content/:content_id" component={FullContent}/>
                <Route path="/package/:package_id" component={PackagesFull}/>
                <Route path="/packages" exact component={Packages}/>
                <Route path="/register" component={RegistrationForm}/>

                {/*{checkDisplay('contents_list') === true ? <ContentsList/> : ''}*/}
            </div>
        );
    }

}

export default MainSection;