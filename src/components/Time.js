import React, {Component} from 'react';
import jMoment from 'moment-jalaali'
import '../scss/Time.scss';

class Loading extends Component {

    getMonthName(i) {
        let month = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند']

        return month[parseInt(i,10)];
    }

    render() {

        if (this.props.time) {
            let t  = jMoment(this.props.time*1000);

            return (

                <div className="time">
                    {t.jDate()} {this.getMonthName(t.jMonth())} {t.jYear()}
                </div>
            );
        }

        return <div></div>
    }
}

export default Loading;
