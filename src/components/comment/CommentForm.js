import React, {Component} from 'react'
import {Field, reduxForm, reset} from 'redux-form'
import {connect} from 'react-redux';
import {createComment} from '../../redux/actions/comments';
import _ from 'lodash';
import '../../scss/CommentForm.scss';
import {Icon} from 'antd';

class CommentForm extends Component {

    submit(values, dispach) {
        this.props.sendData({...values, 'entity_id': this.props.entity_id});

        dispach(reset('comment'));
    }

    render() {

        const {handleSubmit} = this.props;

        let loading = this.props.isLoading;

        let message = "";
        if (this.props.hasError) {
            message = "<p>Sorry! There was an error sending the comment.</p>";
            loading = false;
        }

        return (

            <div className="comment-form">
                <div className="header">
                    شما هم نظری برای این مطلب بنویسید
                </div>
                <form onSubmit={handleSubmit(this.submit.bind(this))}>
                    <span dangerouslySetInnerHTML={{__html: message}}></span>

                    <div className="flex-wrapper">
                        <div className="flex flex-2">

                            <div className="name field-wrapper">
                                {!_.isEmpty(this.props.user) ? this.props.user.name+':' : <Field disabled={loading}
                                                                                                 name="name"
                                                                                                 component="input"
                                                                                                 type="text"
                                                                                                 placeholder="نام شما"
                                />}
                            </div>

                            {this.props.isLogin ? '' : <div className="subject field-wrapper">
                                <Field disabled={loading}
                                       name="email"
                                       component="input"
                                       type="email"
                                       placeholder="ایمیل"
                                />
                            </div>}

                            <div>
                                <button type="submit" disabled={loading}>
                                    {loading ? <Icon type="loading" /> : ''}
                                    ثبت
                                </button>
                            </div>

                        </div>
                        <div className="flex flex-5">

                            <div className="text">
                                <Field disabled={loading}
                                       name="body"
                                       component="textarea"
                                       placeholder="متن پیام"
                                />
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        hasError: state.createCommentsHasError,
        isLoading: state.createCommentsIsLoading,
        user: state.user,
        isLogin: state.isLogin,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        sendData: (data) => dispatch(createComment(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'comment' // a unique identifier for this form
})(CommentForm));
