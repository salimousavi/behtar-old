import React, {Component} from 'react';
import {connect} from 'react-redux';
import {commentsFetchData} from '../../redux/actions/comments';
import {COMMENTS_PATH} from '../../tools/Constants'
import Comment from "./Comment";
import CommentForm from './CommentForm';
import '../../scss/CommentWrapper.scss'

class Contents extends Component {
    componentDidMount() {
        this.props.fetchData(COMMENTS_PATH + "/" + this.props.content_id);
    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the comments</p>;
        }

        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        return (
            <div id="Comments">

                <div className="header">
                    نظرات
                    {/*<div className="count">*/}
                        {/*{this.props.count}*/}
                        {/*نظر*/}
                    {/*</div>*/}
                </div>
                <div className="comments-wrapper">


                    {this.props.comments.map((comment, index) => (
                        <Comment key={index} comment={comment}/>
                    ))}

                </div>

                <div className="comment-form">
                    <CommentForm entity_id={this.props.content_id} />
                </div>


            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        comments: state.comments,
        hasError: state.commentsHasError,
        isLoading: state.commentsIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(commentsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Contents);
