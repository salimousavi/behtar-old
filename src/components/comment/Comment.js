import React, { Component } from 'react';
import '../../scss/Comment.scss'
import Time from '../Time';
class Comment extends Component {

    render() {
        return (
            <div className={"comment"+ (false?' indented':'')} id={"comment_" + this.props.comment.id}><div className="section clearfix">
                <div className="comment-header">

                    <div className="name-wrapper">
                        {/*{this.props.comment.subject}*/}
                        {this.props.comment.username ? this.props.comment.username : 'ناشناس'}
                    </div>

                    <div className="date-wrapper">
                        <Time time={this.props.comment.time} />

                    </div>

                </div>
                <div className="comment-body clearfix">

                    <div className="detail-wrapper">



                        <div className="text-wrapper">

                            <span dangerouslySetInnerHTML={{__html: this.props.comment.body}} />

                        </div>

                    </div>

                </div>

            </div></div>

        );
    }

}

export default Comment;