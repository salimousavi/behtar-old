import React, {Component} from 'react';
import {connect} from 'react-redux';
import {contentsFetchData, contentsType} from '../../redux/actions/contents';
import Content from './Content';
import {CONTENTS_PATH, PAGE_CATEGORIES} from '../../tools/Constants';
import Loading from "../Loading";
import '../../scss/TeaserPage.scss'

class Contents extends Component {

    constructor(props) {
        super(props);

        this.state = {
            display: 1,
        }

        let type = "";

        if (this.props.type) {
            type = this.props.type.toUpperCase();
            type = PAGE_CATEGORIES[type];
        }
        this.props.setContentTypes(type);

        this.props.fetchData(CONTENTS_PATH + "?types=" + type);
    }

    fullDisplay() {
        this.setState({
            display: 3
        })
    }

    quadDisplay() {
        this.setState({
            display: 2
        })
    }

    hexDisplay() {
        this.setState({
            display: 1
        })
    }

    componentDidMount() {


    }

    render() {

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.isLoading) {
            return <Loading/>;
        }

        return (
            <div className="col-sm-">
                <div className="mainbar">
                    <div className="list-page-header clearfix">
                        <div className="pull-right">
                            <div className="sort-by">
                                نمایش به ترتیب:
                                <div className="sort-item active">
                                    استفاده
                                </div>
                                <div className="sort-item">
                                    محبوبیت
                                </div>
                                <div className="sort-item">
                                    بازدید
                                </div>
                                <div className="sort-item">
                                    زمان
                                </div>
                            </div>
                        </div>
                        <div className="pull-left">
                            <div className="show-modes-wrapper">
                                <div onClick={this.fullDisplay.bind(this)} className="show-mode full">
                                    <ul className="list full">
                                    </ul>
                                </div>
                                <div onClick={this.quadDisplay.bind(this)} className="show-mode quad">
                                    <ul className="list quad">
                                        <li className="vertical"></li>
                                        <li className="horizontal"></li>
                                    </ul>
                                </div>
                                <div onClick={this.hexDisplay.bind(this)} className="show-mode hex">
                                    <ul className="list hex">
                                        <li className="vertical"></li>
                                        <li className="vertical"></li>
                                        <li className="vertical"></li>
                                        <li className="horizontal"></li>
                                        <li className="horizontal"></li>
                                        <li className="horizontal"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="list-page-body teaser-mode-block">
                        <div className="items">
                            {this.props.contents.map((content, index) => (
                                <Content display={this.state.display} key={index} content={content}/>
                            ))}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}


const mapStateToProps = (state) => {
    return {
        contents: state.contents,
        hasError: state.contentsHasError,
        isLoading: state.contentsIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(contentsFetchData(url)),
        setContentTypes: (type) => dispatch(contentsType(type))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Contents);
