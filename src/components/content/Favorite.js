import React, {Component} from 'react';
import {connect} from 'react-redux';
import {favoriteFetchData} from '../../redux/actions/favorite';
import {Icon} from 'antd';

class Favorite extends Component {

    componentDidMount() {
        this.props.getFavorites(this.props.content_id);
    }

    toggle() {
        this.props.toggle(this.props.content_id);
    }

    render() {
        return (
            <div className="icon favorite-wrapper">
                <span className="favorite">{this.props.favorite.count}</span>
                {this.props.hasError ?
                    <Icon type="frown" onClick={this.toggle.bind(this)} style={{fontSize: 28, color: '#c4c4c4'}}/>
                    : this.props.isLoading ?
                        <Icon type="loading" style={{fontSize: 28, color: '#c4c4c4'}}/>
                        : this.props.isLogin ?
                            this.props.favorite.isFavorite ?
                                <Icon type="heart" onClick={this.toggle.bind(this)} style={{fontSize: 28, color: '#c4c4c4'}}/>
                                : <Icon type="heart-o" onClick={this.toggle.bind(this)} style={{fontSize: 28, color: '#c4c4c4'}}/>
                            : <Icon type="heart-o" style={{fontSize: 28, color: '#c4c4c4'}}/>
                }
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        favorite: state.favorite,
        hasError: state.favoriteHasError,
        isLoading: state.favoriteIsLoading,
        isLogin: state.isLogin,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFavorites: (content_id, type) => dispatch(favoriteFetchData(content_id, 'get')),
        toggle: (content_id, type) => dispatch(favoriteFetchData(content_id, 'toggle')),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorite);
