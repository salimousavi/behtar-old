import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../../scss/Content.scss'
import Time from '../Time';
import {getCategoryName} from '../../tools/Taxonomies';

class Content extends Component {
    render() {
        /* show mode  :
        *      1 : full : column = 12
        *      2 : quad : column = 6
        *      3 : full : second markup
        */

        if (this.props.display === 1) {
            return (
                <div className="content">
                    <div className="poster posterTeaser hex">
                        <div className="item-wrapper col-sm-3 col-xs-2">
                            <Link to={ "/content/" + this.props.content.id + '/' + this.props.content.name }>
                                <div className="item">
                                    <div className="title">
                                        <span dangerouslySetInnerHTML={{__html: getCategoryName(this.props.content.categories)}}/>
                                    </div>
                                    <div className="image-wrapper">
                                        <img src={this.props.content.image} alt={this.props.content.alt}/>
                                        <div className="caption">
                                        <span>
                                                {this.props.content.name}
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            );
        }

        let showMode = {name: 'full', column:12};

        if (this.props.display === 2 ) {
            showMode = {name: 'quad', column:6};
        }

        return (
            <div className={"content col-sm-"+ showMode.column}>
                <article className={"poster posterTeaser " + showMode.name}>
                    <div className="section clearfix">


                        <div className="header">

                            <Link to={ "/content/" + this.props.content.id + '/' + this.props.content.name}>

                                <div className="name-wrapper">
                                    {this.props.content.name}
                                </div>
                            </Link>

                            <div className="date-wrapper">
                                <Time time={this.props.content.time} />
                            </div>

                            <div className="comment-count-wrapper">
                                { this.props.content.comment_count }
                                نظر
                            </div>

                        </div>

                        <Link to={  "/content/" + this.props.content.id + '/' + this.props.content.name }>

                            <div className="body clearfix">

                                <div className="detail-wrapper">

                                    <div className="text-wrapper">

                                        <span dangerouslySetInnerHTML={{__html: this.props.content.description}}/>

                                    </div>

                                </div>

                                <div className="image-wrapper">
                                    <img src={this.props.content.image} alt={this.props.content.alt}/>
                                </div>

                            </div>
                        </Link>

                    </div>
                </article>
            </div>
        );


    }
}

export default Content;
