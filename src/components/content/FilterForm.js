import React, {Component} from 'react'
import {connect} from 'react-redux';
import {createPackage} from '../../redux/actions/package';
import {contentsFetchData} from '../../redux/actions/contents';
import {Form, InputNumber, Checkbox, TimePicker} from 'antd';
import moment from 'moment'
import {
    TYPES,
    CONTENTS_PATH,
    PAGE_CATEGORIES
} from '../../tools/Constants';
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 18},
};

const formItemLayoutWidth = {
    labelCol: {span: 5},
    wrapperCol: {span: 19},
};

class Filter extends Component {

    constructor(props) {
        super(props);

        let category = "", types;

        if (this.props.category) {
            category = this.props.category.toUpperCase();
            types = TYPES.filter((el) => {
                if (el.category === category) return el;
                return ''
            });
        }

        this.state = {
            types: types,
        };

    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {

                let type = '';
                if (this.props.category) {
                    type = this.props.category.toUpperCase();
                    type = PAGE_CATEGORIES[type];
                }

                let result = Object.assign({}, values);

                if (result.duration !== undefined) {
                    if (result.duration.from !== undefined) {
                        result.duration_from = result.duration.from.format('HH:mm:ss');
                    }
                    if (result.duration.to !== undefined) {
                        result.duration_to = result.duration.to.format('HH:mm:ss');
                    }

                    delete result.duration;
                }

                result.types = type;
                console.log('Received values of form: ', values, result);
                this.props.fetchData(CONTENTS_PATH, result);


            }
        });
    };

    getFieldDecorator = this.props.form.getFieldDecorator;


    selectComponent(name) {
        let options;
        switch (name) {
            case 'Colorful':
                options = [
                    {label: 'رنگی', value: '1'},
                    {label: 'سیاه سفید', value: '0'},
                ];
                return (
                    <fieldset key="colorful-fieldset">
                        <legend>
                            نوع
                        </legend>
                        <FormItem
                            key="colorful">
                            {this.getFieldDecorator('colorful', {
                                initialValue: ['1', '0'],
                            })(
                                <CheckboxGroup options={options} key="colorful"/>
                            )
                            }
                        </FormItem>
                    </fieldset>
                );


            case 'Expensive':
                options = [
                    {label: 'گران', value: '1'},
                    {label: 'ارزان', value: '0'},
                ];
                return (

                    <fieldset key="expensive-fieldset">
                        <legend>
                            نوع هزینه
                        </legend>
                        <FormItem
                            key="expensive">
                            {this.getFieldDecorator('expensive', {
                                initialValue: ['1', '0'],
                            })(
                                <CheckboxGroup options={options} key="expensive"/>
                            )}
                        </FormItem>
                    </fieldset>
                );

            case 'Implemented':
                options = [
                    {label: ' اجرا شده', value: '1'},
                    {label: ' اجرا نشده', value: '0'},
                ];
                return (
                    <fieldset key="implemented-fieldset">
                        <legend>
                            وضعیت
                        </legend>
                        <FormItem
                            key="implemented">
                            {this.getFieldDecorator('implemented', {
                                initialValue: ['1', '0'],
                            })(
                                <CheckboxGroup options={options} key="implemented"/>
                            )}
                        </FormItem>
                    </fieldset>
                );

            case 'Layer':
                options = [
                    {label: ' لایه باز', value: '1'},
                    {label: ' تخت', value: '0'},
                ];
                return (
                    <fieldset key="layer-fieldset">
                        <legend>
                            وضعیت
                        </legend>
                        <FormItem
                            key="layer">
                            {this.getFieldDecorator('layer', {
                                initialValue: ['1', '0'],
                            })(
                                <CheckboxGroup options={options} key="layer"/>
                            )}
                        </FormItem>
                    </fieldset>
                );


            case 'Format':
                return (

                    <fieldset key="format-fieldset">
                        <legend>
                            قالب
                        </legend>
                        <FormItem
                            key="format">
                            {this.getFieldDecorator('format')(
                                <CheckboxGroup options={this.props.formats} key="emplacements"/>
                            )}
                        </FormItem>
                    </fieldset>
                );

            case 'PagesRange':
                return (
                    <fieldset  key="page-fieldset">
                        <legend>
                            تعداد صفحات
                        </legend>
                        <div className="mini-input ant-row">
                            <FormItem className="ant-col-12"
                                      {...formItemLayout} label="از" key="pages_from">
                                {this.getFieldDecorator('pages.from')(
                                    <InputNumber min={1} max={1000}/>
                                )}
                            </FormItem>
                            <FormItem className="ant-col-12"
                                      {...formItemLayout} label="تا" key="pages_to">
                                {this.getFieldDecorator('pages.to')(
                                    <InputNumber min={1} max={1000}/>
                                )}
                            </FormItem>
                        </div>
                    </fieldset>
                );

            case 'SizeRange':
                return (
                    <fieldset key="size-fieldset">
                        <legend>
                            اندازه
                        </legend>
                        <div className="mini-input ant-row shared-label">
                            <FormItem className="ant-col-13"
                                      {...formItemLayoutWidth} label="از" key="width_from">
                                {this.getFieldDecorator('width.from')(
                                    <InputNumber min={1} placeholder="عرض"/>
                                )}
                            </FormItem>
                            <FormItem className="ant-col-11"
                                      key="height_from">
                                {this.getFieldDecorator('height.from')(
                                    <InputNumber min={1} placeholder="ارتفاع"/>
                                )}
                            </FormItem>
                        </div>
                        <div className="mini-input ant-row shared-label">
                            <FormItem className="ant-col-13"
                                      {...formItemLayoutWidth} label="تا" key="width_to">
                                {this.getFieldDecorator('width.to')(
                                    <InputNumber min={1} placeholder="عرض"/>
                                )}
                            </FormItem>
                            <FormItem className="ant-col-11"
                                      key="height_to">
                                {this.getFieldDecorator('height.to')(
                                    <InputNumber min={1} placeholder="ارتفاع"/>
                                )}
                            </FormItem>
                        </div>
                    </fieldset>
                );

            case 'DurationRange':
                return (
                    <fieldset key="time-fieldset">
                        <legend>مدت زمان

                        </legend>
                        <div className="mini-input ant-row">
                            <FormItem className="ant-col-12"
                                      {...formItemLayout} label="از" key="duration_from">
                                {this.getFieldDecorator('duration.from')(
                                    <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} placeholder=""/>
                                )}
                            </FormItem>
                            <FormItem className="ant-col-12"
                                      {...formItemLayout} label="تا" key="duration_to">
                                {this.getFieldDecorator('duration.to')(
                                    <TimePicker defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} placeholder=""/>
                                )}
                            </FormItem>
                        </div>
                    </fieldset>
                );

            default:
                return;
        }
    }

    getFormComponents() {

        let filters = [];
        this.state.types.map(el => {
            return el.filters.map(f => filters.push(f))
        });

        filters = filters.filter(function (item, index, inputArray) {
            return inputArray.indexOf(item) === index;
        });

        let fields = [];
        filters.map(el => fields.push(this.selectComponent(el)));
        return fields;
    }

    render() {


        let loading = this.props.isLoading;

        let message = "";
        if (this.props.hasError) {
            message = "<p>Sorry! There was an error sending the package.</p>";
            loading = false;
        }


        return (
            <div className="filter-block block">
                <Form onSubmit={this.handleSubmit}>
                    <span dangerouslySetInnerHTML={{__html: message}}></span>

                    {this.getFormComponents()}
                    <div>
                        <button type="submit" disabled={loading}>فیلتر</button>
                    </div>
                </Form>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        hasError: state.createContentHasError,
        isLoading: state.createContentIsLoading,
        success: state.createContentSuccess,
        content: state.content,
        formats: state.formats,
        formatsHasError: state.formatsHasError,
        formatsIsLoading: state.formatsIsLoading,
        websites: state.websites,
        emplacements: state.emplacements,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        sendData: (data) => dispatch(createPackage(data)),
        fetchData: (url, data) => dispatch(contentsFetchData(url, data))
    }
}

const FilterForm = Form.create()(Filter);

export default connect(mapStateToProps, mapDispatchToProps)(FilterForm);