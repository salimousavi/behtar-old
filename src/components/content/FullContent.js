import React, {Component} from 'react';
import {connect} from 'react-redux';
import {contentFetchData, deleteContent} from '../../redux/actions/content';
import {CONTENTS_PATH} from '../../tools/Constants';
import Comments from '../comment/Comments';
import Video from "react-h5-video";
import AudioPlayer from 'react-responsive-audio-player';
import Loading from "../Loading";
import Favorite from './Favorite';
import Time from '../Time';
import AddToCart from '../cart/AddToCart';
import {Icon, Popconfirm, message, Tooltip} from 'antd';
import '../../scss/FullContent.scss'
import {Link, Redirect} from 'react-router-dom';
import _ from 'lodash'
import {getCategoryName, getEmplacementsName, getFormatName, getWebsitesName} from "../../tools/Taxonomies";
import moment from 'moment'

class Content extends Component {

    constructor(props) {
        super(props);
        this.getContent(this.props.match.params.content_id);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.match.params.content_id !== nextProps.match.params.content_id) {
            this.getContent(nextProps.match.params.content_id);
        }
    }

    getContent(content_id) {
        this.props.fetchData(CONTENTS_PATH + "/" + content_id);
    }

    download() {
        window.open(this.props.content.file, '_self');
    }

    delete() {
        this.props.deleteContent(this.props.content.id);
    }

    render() {
        if (this.props.deleteSuccess) {
            message.config({
                top: 50,
                duration: 2,
            });
            message.success('محتوا حذف شد.');
            return <Redirect to={'/'}/>;
        }

        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the contents</p>;
        }

        if (this.props.isLoading) {
            return <Loading/>;
        }

        let image =
            <div className="image-wrapper">
                <img src={this.props.content.image} alt={this.props.content.alt}/>
            </div>;

        let audio = '';
        if (!_.isEmpty(this.props.content.audio)) {
            let playlist = this.props.content.audio.map(el => {
                return {url: el.url, displayText: el.name}
            });
            audio = <div className="audio-wrapper" >
                <AudioPlayer playlist={playlist} hideBackSkip={true} cycle={false}/>
            </div>;
        }


        let video = '';
        if (!_.isEmpty(this.props.content.video)) {
            video =
                <div className="video-wrapper">

                    <Video
                        sources={[this.props.content.video[0].url]}
                        poster={this.props.content.image}
                        width="100%"
                        height="auto"
                        controlPanelStyle="fixed"
                    >
                        <h3 className="video-logo pull-right"><a href="http://glexe.com"
                                                                 rel="noopener noreferrer"
                                                                 target="_blank">LOGO</a></h3>
                    </Video>
                </div>;
            image = '';
        }

        let link = '';
        if (this.props.content.file) {
            // link =
            //     <a className="download-btn" href={this.props.content.file} download="proposed_file_name">
            //         دریافت فایل
            //     </a>
            link =
                <div className="content-item files-wrapper">
                    <span className="label">
                        دریافت فایل‌ها:
                    </span>
                    <span className="data">
                        {this.props.content.image_edit.map(el => {
                            return (
                            <a key={el.response} className="" href={el.url} title={el.name} download="proposed_file_name">
                                <Icon type="file-jpg" />
                                دریافت تصویر در اندازه اصلی
                            </a>
                            );
                        })}
                        {this.props.content.audio.map(el => {
                            return (
                            <a key={el.response} className="" href={el.url} title={el.name} download="proposed_file_name">
                                <Icon type="notification" />
                                دریافت فایل صوتی
                            </a>
                            );
                        })}
                        {this.props.content.video.map(el => {
                            return (
                            <a key={el.response} className="" href={el.url} title={el.name} download="proposed_file_name">
                                <Icon type="video-camera" />
                                دریافت فایل ویدیویی
                            </a>
                            );
                        })}
                        {this.props.content.file.map(el => {
                            return (
                            <a key={el.response} className="" href={el.url} title={el.name} download="proposed_file_name">
                                <Icon type="file" />
                                {el.name}
                            </a>
                            );
                        })}
                    </span>
                </div>
        }

        // $.fn.scrollFun = function () {
        //     $(this).click(function (e) {
        //         let h = $(this).attr('href'),
        //             target;
        //
        //         if (h.charAt(0) === '#' && h.length > 1 && (target = $(h)).length > 0) {
        //             let pos = Math.max(target.offset().top, 0);
        //             e.preventDefault();
        //             $('body,html').animate({
        //                 scrollTop: pos
        //             }, 'slow', 'swing');
        //         }
        //     });
        // };
        //
        // $('#comment-link').scrollFun();

        return (
            <div className="poster posterFull">
                <div className="">
                    <div className="col-sm-">
                        <div className="mainbar clearfix">

                            <div className="header">
                                <div className="sub-header clearfix">

                                    <div className="name-wrapper">
                                        {this.props.content.name}
                                    </div>

                                    <div className="pull-left left-icons">

                                        <Favorite content_id={this.props.match.params.content_id} />

                                        <div id="comment-link" href="Comments" className="icon comment-count-wrapper">
                                            <span> {this.props.content.comment_count} </span>
                                            <Tooltip title="تعداد پیام‌ها">
                                                <Icon type="message" style={{fontSize: 28, color: '#c4c4c4'}} />
                                            </Tooltip>
                                        </div>

                                        {this.props.isAdmin ? <div className="icon action-cart">
                                            <AddToCart content={this.props.content}/>
                                        </div> : ''}

                                        {this.props.isAdmin ?
                                            <Popconfirm title="آیا از حذف این محتوا مطمئن هستید؟"  onConfirm={this.delete.bind(this)} okText="بله" cancelText="لغو">
                                                <div className="icon">
                                                    <Tooltip title="حذف محتوا">
                                                        {this.props.deleteLoading ?
                                                        <Icon type="loading" style={{fontSize: 28, color: '#c4c4c4'}} />
                                                        : <Icon type="close-circle-o" style={{fontSize: 28, color: '#c4c4c4'}} />}
                                                    </Tooltip>
                                                </div>
                                            </Popconfirm> : ''}

                                        {this.props.isAdmin ?
                                            <div className="icon">
                                                <Link to={'/content/'+this.props.content.id+'/edit'}>
                                                    <Tooltip title="ویرایش محتوا">
                                                        <Icon type="edit" style={{fontSize: 28, color: '#c4c4c4'}} />
                                                    </Tooltip>
                                                </Link>
                                            </div> : ''}

                                    </div>
                                </div>

                                <div className="date-wrapper">
                                    <Time time={this.props.content.time}/>
                                </div>
                            </div>

                            <div className="body">

                                {video}
                                {image}
                                {audio}

                                <div className="detail-wrapper">
                                    <span dangerouslySetInnerHTML={{__html: this.props.content.description}}/>
                                </div>

                                {!_.isEmpty(this.props.content.categories) ?
                                    <div className="content-item">
                                    <span className="label">
                                        دسته بندی:
                                    </span>
                                    <span dangerouslySetInnerHTML={{__html: getCategoryName(this.props.content.categories)}}/>
                                </div>
                                    : '' }

                                {!_.isEmpty(this.props.content.emplacements) ?
                                    <div className="content-item">
                                    <span className="label">
                                        محل استفاده:
                                    </span>
                                    <span dangerouslySetInnerHTML={{__html: getEmplacementsName(this.props.content.emplacements)}}/>
                                </div>
                                    : '' }

                                {!_.isEmpty(this.props.content.websites) ?
                                    <div className="content-item">
                                    <span className="label">
                                        منتشرشده در:
                                    </span>
                                    <span dangerouslySetInnerHTML={{__html: getWebsitesName(this.props.content.websites)}}/>
                                </div>
                                    : '' }

                                {!_.isEmpty(this.props.content.format) ?
                                    <div className="content-item">
                                    <span className="label">
                                        قالب بندی:
                                    </span>
                                    <span dangerouslySetInnerHTML={{__html: getFormatName(this.props.content.format)}}/>
                                </div>
                                    : '' }

                                {this.props.content.colorful === '0' || this.props.content.colorful === '1' ?
                                    <div className="content-item">
                                    <span className="label">
                                        رنگ بندی:
                                    </span>
                                    <span>
                                        {this.props.content.colorful === '0' ? 'سیاه و سفید' : 'تمام رنگی'}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.pages ?
                                    <div className="content-item">
                                    <span className="label">
                                        تعداد صفحات:
                                    </span>
                                    <span>
                                        {this.props.content.pages}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.width ?
                                    <div className="content-item">
                                    <span className="label">
                                        عرض:
                                    </span>
                                    <span>
                                        {this.props.content.width}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.height ?
                                    <div className="content-item">
                                    <span className="label">
ارتفاع:
                                    </span>
                                    <span>
                                        {this.props.content.height}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.duration ?
                                    <div className="content-item">
                                    <span className="label">
                                        مدت زمان:
                                    </span>
                                    <span>
                                        {moment.utc(this.props.content.duration*1000).format('HH:mm:ss')}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.expensive === '0' || this.props.content.expensive === '1' ?
                                    <div className="content-item">
                                    <span className="label">
                                        قیمت ایده:
                                    </span>
                                    <span>
                                        {this.props.content.expensive === '0' ? 'ارزان' : 'گران'}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.implemented === '0' || this.props.content.implemented === '1' ?
                                    <div className="content-item">
                                    <span className="label">
                                        وضعیت ایده:
                                    </span>
                                    <span>
                                        {this.props.content.implemented === '0' ? 'اجرا نشده' : 'اجرا شده'}
                                    </span>
                                </div>
                                    : '' }

                                {this.props.content.layer === '0' || this.props.content.layer === '1' ?
                                    <div className="content-item">
                                    <span className="label">
                                        نوع فایل:
                                    </span>
                                    <span>
                                        {this.props.content.layer === '0' ? 'تخت' : 'لایه باز'}
                                    </span>
                                </div>
                                    : '' }
                                {/*download link*/}
                                {link}

                            </div>

                            <Comments count={this.props.content.comment_count}
                                      content_id={this.props.match.params.content_id}/>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}


const mapStateToProps = (state) => {
    return {
        content: state.content,
        hasError: state.contentHasError,
        isLoading: state.contentIsLoading,
        isAdmin: state.isAdmin,
        deleteError: state.deleteContentHasError,
        deleteLoading: state.deleteContentIsLoading,
        deleteSuccess: state.deleteContentSuccess,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(contentFetchData(url)),
        deleteContent: (id) => dispatch(deleteContent(id)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
