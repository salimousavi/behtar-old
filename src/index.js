import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './redux/store/configureStore';
import App from './components/App';
import { BrowserRouter , Route } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';
import './scss/audioplayer-customized.scss';
import './scss/react-html5-video-customized.scss';

export const store = configureStore();

ReactDOM.render(
    <Provider store={store} >
        <BrowserRouter>
            <div>
                <Route name="App" path="/" component={App} />
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
