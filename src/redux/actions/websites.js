import { BASE_URL, WEBSITES_PATH } from '../../tools/Constants'

export function websitesHasError(bool) {
    return {
        type: 'WEBSITES_HAS_ERRORED',
        hasError: bool
    };
}

export function websitesIsLoading(bool) {
    return {
        type: 'WEBSITES_IS_LOADING',
        isLoading: bool
    };
}

export function websitesFetchDataSuccess(websites) {
    return {
        type: 'WEBSITES_FETCH_DATA_SUCCESS',
        websites
    };
}

export function websitesFetchData(path = WEBSITES_PATH) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(websitesIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(websitesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((websites) => dispatch(websitesFetchDataSuccess(websites)))
            .catch(() => dispatch(websitesHasError(true)));
    };
}
