import { BASE_URL, CONTENTS_PATH } from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function contentsType(type) {
    return {
        type: 'CONTENTS_TYPE',
        contentsType: type
    };
}

export function contentsHasError(bool) {
    return {
        type: 'CONTENTS_HAS_ERRORED',
        hasError: bool
    };
}

export function contentsIsLoading(bool) {
    return {
        type: 'CONTENTS_IS_LOADING',
        isLoading: bool
    };
}

export function contentsFetchDataSuccess(contents) {
    return {
        type: 'CONTENTS_FETCH_DATA_SUCCESS',
        contents
    };
}

export function contentsFetchData(path, data=null) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(contentsFetchDataSuccess([]));
        dispatch(contentsIsLoading(true));
        dispatch(contentsHasError(false));
        if (data) {
            data = {
                method: 'POST',
                body: JSON.stringify(data),
            }
        }

        fetch(URL, Header(data))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(contentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((contents) => dispatch(contentsFetchDataSuccess(contents)))
            .catch(() => dispatch(contentsHasError(true)));
    };
}


export function deleteContents(ids) {
    let URL = BASE_URL + CONTENTS_PATH;
    return (dispatch) => {
        dispatch(contentsIsLoading(true));

        fetch(URL, Header({
            method: 'DELETE',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(contentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((contents) => dispatch(contentsFetchData(CONTENTS_PATH)))
            .catch(() => dispatch(contentsHasError(true)));
    };
}

export function changeStatusContents(ids, type='publish') {
    let URL = BASE_URL + CONTENTS_PATH + '/' + type;
    return (dispatch) => {
        dispatch(contentsIsLoading(true));

        fetch(URL, Header({
            method: 'PUT',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(contentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((contents) => dispatch(contentsFetchData(CONTENTS_PATH)))
            .catch(() => dispatch(contentsHasError(true)));
    };
}
