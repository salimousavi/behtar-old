import { BASE_URL, FAVORITE_PATH } from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function favoriteHasError(bool) {
    return {
        type: 'FAVORITE_HAS_ERRORED',
        hasError: bool
    };
}

export function favoriteIsLoading(bool) {
    return {
        type: 'FAVORITE_IS_LOADING',
        isLoading: bool
    };
}

export function favoriteFetchDataSuccess(favorite) {
    return {
        type: 'FAVORITE_FETCH_DATA_SUCCESS',
        favorite
    };
}

export function favoriteFetchData(content_id, type) {
    let URL = BASE_URL + FAVORITE_PATH + '/' + content_id;
    return (dispatch) => {
        dispatch(favoriteIsLoading(true));
        dispatch(favoriteHasError(false));
        let method = type === 'toggle' ? 'POST' : 'GET';
        fetch(URL, Header({method: method})).then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(favoriteIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((favorite) => dispatch(favoriteFetchDataSuccess(favorite)))
            .catch(() => dispatch(favoriteHasError(true)));
    };
}
