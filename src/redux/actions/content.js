import {BASE_URL, CONTENT_PATH, CONTENTS_PATH, FILES_PATH} from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function deleteContentHasError(bool) {
    return {
        type: 'DELETE_CONTENT_HAS_ERRORED',
        deleteHasError: bool
    };
}

export function deleteContentIsLoading(bool) {
    return {
        type: 'DELETE_CONTENT_IS_LOADING',
        deleteIsLoading: bool
    };
}

export function deleteContentSuccess(bool) {
    return {
        type: 'DELETE_CONTENT_SUCCESS',
        deleteSuccess: bool
    };
}

export function createContentHasError(bool) {
    return {
        type: 'CREATE_CONTENT_HAS_ERRORED',
        createHasError: bool
    };
}

export function createContentIsLoading(bool) {
    return {
        type: 'CREATE_CONTENT_IS_LOADING',
        createIsLoading: bool
    };
}

export function createContentSuccess(bool) {
    return {
        type: 'CREATE_CONTENT_SUCCESS',
        createSuccess: bool
    };
}

export function contentHasError(bool) {
    return {
        type: 'CONTENT_HAS_ERRORED',
        hasError: bool
    };
}

export function contentIsLoading(bool) {
    return {
        type: 'CONTENT_IS_LOADING',
        isLoading: bool
    };
}

export function contentFetchDataSuccess(content) {
    return {
        type: 'CONTENT_FETCH_DATA_SUCCESS',
        content
    };
}

export function contentFetchData(path) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(contentFetchDataSuccess({}));
        dispatch(contentIsLoading(true));
        dispatch(contentHasError(false));

        fetch(URL, Header())
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(contentIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((content) => dispatch(contentFetchDataSuccess(content)))
            // .catch(() => dispatch(contentHasError(true)));
    };
}

export function createContent(data, method = 'POST') {
    console.log(data, 'eee');
    let url = BASE_URL + CONTENT_PATH;
    return (dispatch) => {
        dispatch(createContentIsLoading(true));

        fetch(url, Header({
            method: 'POST',
            body: JSON.stringify(data),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createContentIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                dispatch(contentFetchDataSuccess(p));
                dispatch(createContentSuccess(true));
                dispatch(createContentSuccess(false));
            })
            .catch((response) => dispatch(createContentHasError(true)));
    };
}

export function updateContent(data) {
    console.log(data, 'eee');
    let url = BASE_URL + CONTENT_PATH;
    return (dispatch) => {
        dispatch(createContentIsLoading(true));

        fetch(url, Header({
            method: 'PUT',
            body: JSON.stringify(data),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createContentIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                dispatch(createContentSuccess(true));
                dispatch(createContentSuccess(false));
                dispatch(contentFetchDataSuccess(p));
            })
            .catch((response) => dispatch(createContentHasError(true)));
    };
}

export function deleteContent(id) {
    let url = BASE_URL + CONTENTS_PATH + '/' + id;
    return (dispatch) => {
        dispatch(deleteContentIsLoading(true));
        dispatch(deleteContentHasError(false));

        fetch(url, Header({
            method: 'DELETE',
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(deleteContentIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                dispatch(deleteContentSuccess(true));
                dispatch(deleteContentSuccess(false));

            })
            .catch((response) => dispatch(deleteContentHasError(true)));
    };

}

export function deleteFiles(ids) {
    let URL = BASE_URL + FILES_PATH;
    return (dispatch) => {
        fetch(URL, Header({
            method: 'DELETE',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                return response;
            })
            .then((response) => response.json())
            .catch((response) =>  { throw Error(response.statusText)});
    };
}