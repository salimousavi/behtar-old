export function setPageIsAdmin(bool=false) {
    return (dispatch) => {
        dispatch({
            type: 'PAGE_IS_ADMIN',
            pageIsAdmin: bool
        })
    };
}
