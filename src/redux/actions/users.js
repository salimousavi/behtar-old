import { BASE_URL, USERS_PATH } from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function changeStatusUsersHasError(bool) {
    return {
        type: 'CHANGE_STATUS_USERS_HAS_ERROR',
        hasError: bool
    };
}

export function changeStatusUsersIsLoading(bool) {
    return {
        type: 'CHANGE_STATUS_USERS_IS_LOADING',
        isLoading: bool
    };
}

export function usersHasError(bool) {
    return {
        type: 'USERS_HAS_ERROR',
        hasError: bool
    };
}

export function usersIsLoading(bool) {
    return {
        type: 'USERS_IS_LOADING',
        isLoading: bool
    };
}

export function usersFetchDataSuccess(users) {
    return {
        type: 'USERS_FETCH_DATA_SUCCESS',
        users
    };
}

export function changeStatusUsers(ids, type='activate') {
    let URL = BASE_URL + USERS_PATH + '/' + type;

    console.log(ids);
    return (dispatch) => {
        dispatch(changeStatusUsersIsLoading(true));
        dispatch(changeStatusUsersHasError(false));

        fetch(URL, Header({
            method: 'POST',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(changeStatusUsersIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((users) => dispatch(usersFetchData()))
            .catch(() => dispatch(changeStatusUsersHasError(true)));
    }
}

export function deleteUsers(ids) {
    let URL = BASE_URL + USERS_PATH;
    return (dispatch) => {
        dispatch(usersIsLoading(true));
        dispatch(usersHasError(false));

        fetch(URL, Header({
            method: 'DELETE',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(usersIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((users) => dispatch(usersFetchData()))
            .catch(() => dispatch(usersHasError(true)));
    };
}

export function usersFetchData(data=null) {
    let URL = BASE_URL + USERS_PATH;
    return (dispatch) => {
        dispatch(usersIsLoading(true));
        dispatch(usersHasError(false));
        if (data) {
            data = {
                method: 'POST',
                body: JSON.stringify(data),
            }
        }

        fetch(URL, Header())
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(usersIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((users) => dispatch(usersFetchDataSuccess(users)))
            .catch(() => dispatch(usersHasError(true)));
    };
}
