import {BASE_URL} from '../../tools/Constants'

export function packagesHasError(bool) {
    return {
        type: 'PACKAGES_HAS_ERRORED',
        hasError: bool
    };
}

export function packagesIsLoading(bool) {
    return {
        type: 'PACKAGES_IS_LOADING',
        isLoading: bool
    };
}

export function packagesFetchDataSuccess(packages) {
    return {
        type: 'PACKAGES_FETCH_DATA_SUCCESS',
        packages
    };
}

export function packagesFetchData(path) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(packagesIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(packagesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((packages) => dispatch(packagesFetchDataSuccess(packages)))
            .catch(() => dispatch(packagesHasError(true)));
    };
}
