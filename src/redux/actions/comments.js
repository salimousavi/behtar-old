import {BASE_URL, COMMENTS_PATH} from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function commentsHasError(bool) {
    return {
        type: 'COMMENTS_HAS_ERRORED',
        hasError: bool
    };
}

export function commentsIsLoading(bool) {
    return {
        type: 'COMMENTS_IS_LOADING',
        isLoading: bool
    };
}

export function createCommentsHasError(bool) {
    return {
        type: 'CREATE_COMMENTS_HAS_ERRORED',
        hasError: bool
    };
}

export function createCommentsIsLoading(bool) {
    return {
        type: 'CREATE_COMMENTS_IS_LOADING',
        isLoading: bool
    };
}

export function commentsFetchDataSuccess(comments) {
    return {
        type: 'COMMENTS_FETCH_DATA_SUCCESS',
        comments
    };
}

export function commentsFetchData(path) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(commentsIsLoading(true));

        fetch(URL, Header({method: 'GET'}))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(commentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((comments) => dispatch(commentsFetchDataSuccess(comments)))
            .catch(() => dispatch(commentsHasError(true)));
    };
}

export function deleteComments(ids) {
    let URL = BASE_URL + COMMENTS_PATH;
    return (dispatch) => {
        dispatch(commentsIsLoading(true));

        fetch(URL, Header({
            method: 'DELETE',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(commentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((comments) => dispatch(commentsFetchData(COMMENTS_PATH)))
            .catch(() => dispatch(commentsHasError(true)));
    };
}

export function changeStatusComments(ids, type='publish') {
    let URL = BASE_URL + COMMENTS_PATH + '/' + type;
    return (dispatch) => {
        dispatch(commentsIsLoading(true));

        fetch(URL, Header({
            method: 'PUT',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(commentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((comments) => dispatch(commentsFetchData(COMMENTS_PATH)))
            .catch(() => dispatch(commentsHasError(true)));
    };
}

export function createComment(data) {
    let url = BASE_URL + COMMENTS_PATH;

    return (dispatch) => {
        dispatch(createCommentsIsLoading(true));

        let h = Header({
            method: 'POST',
            body: JSON.stringify(data),
        });

        fetch(url, h)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createCommentsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((comments) => dispatch(commentsFetchDataSuccess(comments)))
            .catch((response) => dispatch(createCommentsHasError(true)));
    };

}
