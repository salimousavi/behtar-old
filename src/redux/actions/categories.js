import { BASE_URL, CATEGORIES_PATH } from '../../tools/Constants'

export function categoriesHasError(bool) {
    return {
        type: 'CATEGORIES_HAS_ERROR',
        hasError: bool
    };
}

export function categoriesIsLoading(bool) {
    return {
        type: 'CATEGORIES_IS_LOADING',
        isLoading: bool
    };
}

export function categoriesFetchDataSuccess(categories) {
    return {
        type: 'CATEGORIES_FETCH_DATA_SUCCESS',
        categories
    };
}

export function categoriesFetchData() {
    let URL = BASE_URL + CATEGORIES_PATH;
    return (dispatch) => {
        dispatch(categoriesIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(categoriesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((categories) => dispatch(categoriesFetchDataSuccess(categories)))
            .catch(() => dispatch(categoriesHasError(true)));
    };
}