import { BASE_URL, EMPLACEMENTS_PATH } from '../../tools/Constants'

export function emplacementsHasError(bool) {
    return {
        type: 'EMPLACEMENTS_HAS_ERRORED',
        hasError: bool
    };
}

export function emplacementsIsLoading(bool) {
    return {
        type: 'EMPLACEMENTS_IS_LOADING',
        isLoading: bool
    };
}

export function emplacementsFetchDataSuccess(emplacements) {
    return {
        type: 'EMPLACEMENTS_FETCH_DATA_SUCCESS',
        emplacements
    };
}

export function emplacementsFetchData(path = EMPLACEMENTS_PATH) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(emplacementsIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(emplacementsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((emplacements) => dispatch(emplacementsFetchDataSuccess(emplacements)))
            .catch(() => dispatch(emplacementsHasError(true)));
    };
}
