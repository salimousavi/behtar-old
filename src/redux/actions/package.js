import {BASE_URL, PACKAGES_PATH} from '../../tools/Constants'
import {Header} from '../../tools/Header';
import {cartFetchDataSuccess} from './cart';
export function createPackageHasError(bool) {
    return {
        type: 'CREATE_PACKAGE_HAS_ERRORED',
        hasError: bool
    };
}

export function createPackageIsLoading(bool) {
    return {
        type: 'CREATE_PACKAGE_IS_LOADING',
        isLoading: bool
    };
}

export function packageHasError(bool) {
    return {
        type: 'PACKAGE_HAS_ERRORED',
        hasError: bool
    };
}

export function packageIsLoading(bool) {
    return {
        type: 'PACKAGE_IS_LOADING',
        isLoading: bool
    };
}

export function packageFetchDataSuccess(p) {
    console.log(p, 'okokokokkok');
    return {
        type: 'PACKAGE_FETCH_DATA_SUCCESS',
        p: p
    };
}

export function packageFetchData(path) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(packageIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                // dispatch(packageIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => dispatch(packageFetchDataSuccess(p)))
            .then(() => dispatch(packageIsLoading(false)))
            .catch(() => dispatch(packageHasError(true)));
    };
}


export function packageDownload(path) {
    let URL = BASE_URL + path;

    return (dispatch) => {

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                // dispatch(packageIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => dispatch(window.open(p, '_self')))
            .catch(() => console.log('error'));
    };
}

export function createPackageSuccess(bool) {
    return {
        type: 'CREATE_PACKAGE_SUCCESS',
        createSuccess: bool
    };
}

export function createPackage(data) {
    let url = BASE_URL + PACKAGES_PATH;

    return (dispatch) => {
        dispatch(createPackageIsLoading(true));

        fetch(url, Header({
            method: 'POST',
            body: JSON.stringify(data),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createPackageIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                dispatch(packageFetchDataSuccess(p));
                dispatch(cartFetchDataSuccess([]));
                dispatch(createPackageSuccess(true));
                dispatch(createPackageSuccess(false));
            })
            .catch((response) => dispatch(createPackageHasError(true)));
    };

}
