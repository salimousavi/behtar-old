import { BASE_URL, SLIDES_PATH } from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function slideHasError(bool) {
    return {
        type: 'SLIDE_HAS_ERROR',
        hasError: bool
    };
}

export function slideIsLoading(bool) {
    return {
        type: 'SLIDE_IS_LOADING',
        isLoading: bool
    };
}

export function slideFetchDataSuccess(slide) {
    return {
        type: 'SLIDE_FETCH_DATA_SUCCESS',
        slide
    };
}

export function slidesHasError(bool) {
    return {
        type: 'SLIDES_HAS_ERROR',
        hasError: bool
    };
}

export function slidesIsLoading(bool) {
    return {
        type: 'SLIDES_IS_LOADING',
        isLoading: bool
    };
}

export function slidesFetchDataSuccess(slides) {
    return {
        type: 'SLIDES_FETCH_DATA_SUCCESS',
        slides
    };
}

export function createSlideHasError(bool) {
    return {
        type: 'CREATE_SLIDE_HAS_ERROR',
        createHasError: bool
    };
}

export function createSlideIsLoading(bool) {
    return {
        type: 'CREATE_SLIDE_IS_LOADING',
        createIsLoading: bool
    };
}

export function createSlideSuccess(bool) {
    return {
        type: 'CREATE_SLIDE_SUCCESS',
        createSuccess: bool
    };
}

export function slideFetchData(id) {
    let URL = BASE_URL + SLIDES_PATH + '/' + id;
    return (dispatch) => {
        dispatch(slidesIsLoading(true));

        fetch(URL, Header())
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(slidesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((slide) => dispatch(slideFetchDataSuccess(slide)))
            .catch(() => dispatch(slideHasError(true)));
    };
}

export function slidesFetchData() {
    let URL = BASE_URL + SLIDES_PATH;
    return (dispatch) => {
        dispatch(slidesIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(slidesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((slides) => dispatch(slidesFetchDataSuccess(slides)))
            .catch(() => dispatch(slidesHasError(true)));
    };
}

export function saveChangesSlides(data) {
    let URL = BASE_URL + SLIDES_PATH + '/save_changes';
    return (dispatch) => {
        dispatch(slidesIsLoading(true));

        fetch(URL, Header({
            method: 'POST',
            body: JSON.stringify({data}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(slidesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((slides) => dispatch(slidesFetchDataSuccess(slides)))
            .catch(() => dispatch(slidesHasError(true)));
    };
}

export function deleteSlides(ids) {
    let URL = BASE_URL + SLIDES_PATH;
    return (dispatch) => {
        dispatch(slidesIsLoading(true));

        fetch(URL, Header({
            method: 'DELETE',
            body: JSON.stringify({ids: ids}),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(slidesIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((slides) => dispatch(slidesFetchDataSuccess(slides)))
            .catch(() => dispatch(slidesHasError(true)));
    };
}

export function createSlide(data, method = 'POST') {
    let url = BASE_URL + SLIDES_PATH;
    return (dispatch) => {
        dispatch(createSlideIsLoading(true));

        fetch(url, Header({
            method: method,
            body: JSON.stringify(data),
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createSlideIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                dispatch(slidesFetchDataSuccess(p));
                dispatch(createSlideSuccess(true));
                dispatch(createSlideSuccess(false));
            })
            .catch((response) => dispatch(createSlideHasError(true)));
    };
}
