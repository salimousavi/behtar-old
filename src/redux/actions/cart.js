import {BASE_URL} from '../../tools/Constants'
import {Header} from '../../tools/Header';

export function cartHasError(bool) {
    return {
        type: 'CART_HAS_ERRORED',
        hasError: bool
    };
}

export function cartIsLoading(bool) {
    return {
        type: 'CART_IS_LOADING',
        isLoading: bool
    };
}

export function addToCartHasError(bool) {
    return {
        type: 'ADD_TO_CART_HAS_ERRORED',
        hasError: bool
    };
}

export function addToCartIsLoading(bool) {
    return {
        type: 'ADD_TO_CART_IS_LOADING',
        isLoading: bool
    };
}

// export function addToCartSuccess(cart) {
//     // console.log({content, ...cart}, content, cart);
//     // cart.push(content);
//     // let result = Object.assign(cart, {content);
//     // console.log(result);
//     console.log(cart, 'ppoooooooooooo');
//     // dispatch(cartFetchDataSuccess(cart));
//
//     return {
//         type: 'CART_FETCH_DATA_SUCCESS',
//         cart,
//     };
// }

export function cartFetchDataSuccess(cart) {
    return {
        type: 'CART_FETCH_DATA_SUCCESS',
        cart
    };
}

export function cartFetchData(path) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(cartIsLoading(true));

        fetch(URL,Header())
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(cartIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((cart) => dispatch(cartFetchDataSuccess(cart)))
            .catch(() => dispatch(cartHasError(true)));
    };
}

export function addToCart(path, id) {
    let URL = BASE_URL + path;

    let body = new FormData();
    body.append('id', id);

    return (dispatch) => {
        dispatch(addToCartIsLoading(true));

        fetch(URL, Header({
            method: 'POST',
            body: body,
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(addToCartIsLoading(false));
                // dispatch(cartIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            // .then((cart) => dispatch(addToCartSuccess(cart)))
            .then((cart) => dispatch(cartFetchDataSuccess(cart)))
            // .then((content) => dispatch(cartFetchData(CART_PATH)))
            .catch(() => dispatch(addToCartHasError(true)));
    };
}

export function removeFromCart(path, id) {
    let URL = BASE_URL + path + '/' + id;

    return (dispatch) => {
        dispatch(addToCartIsLoading(true));

        fetch(URL, Header({
            method: 'DELETE',
        }))
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(addToCartIsLoading(false));
                // dispatch(cartIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((cart) => dispatch(cartFetchDataSuccess(cart)))
            .catch(() => dispatch(addToCartHasError(true)));
    };
}
