import { BASE_URL, FORMATS_PATH } from '../../tools/Constants'

export function formatsHasError(bool) {
    return {
        type: 'FORMATS_HAS_ERRORED',
        hasError: bool
    };
}

export function formatsIsLoading(bool) {
    return {
        type: 'FORMATS_IS_LOADING',
        isLoading: bool
    };
}

export function formatsFetchDataSuccess(formats) {
    return {
        type: 'FORMATS_FETCH_DATA_SUCCESS',
        formats
    };
}

export function formatsFetchData(path = FORMATS_PATH) {
    let URL = BASE_URL + path;
    return (dispatch) => {
        dispatch(formatsIsLoading(true));

        fetch(URL)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(formatsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((formats) => dispatch(formatsFetchDataSuccess(formats)))
            .catch(() => dispatch(formatsHasError(true)));
    };
}
