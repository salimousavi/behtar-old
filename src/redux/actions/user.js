import {BASE_URL, REGISTER_PATH, USER_PATH} from '../../tools/Constants'
import cookie from 'react-cookies';

export function loginError(bool) {
    return {
        type: 'LOGIN_ERROR',
        loginError: bool
    };
}

export function userHasError(bool) {
    return {
        type: 'USER_HAS_ERRORED',
        hasError: bool
    };
}

export function userIsLoading(bool) {
    return {
        type: 'USER_IS_LOADING',
        isLoading: bool
    };
}

export function createUserHasError(bool) {
    return {
        type: 'CREATE_USER_HAS_ERRORED',
        hasError: bool
    };
}

export function createUserIsLoading(bool) {
    return {
        type: 'CREATE_USER_IS_LOADING',
        isLoading: bool
    };
}

export function createUserSuccess(bool) {
    return {
        type: 'CREATE_USER_SUCCESS',
        createSuccess: bool
    };
}

export function logout() {
    cookie.remove('token', {path: '/'});
    return (dispatch) => {
        dispatch(isLogin(false));
        dispatch(isAdmin(false));
        dispatch(userFetchDataSuccess({}));
    }
}

export function userFetchDataSuccess(user) {
    cookie.save('token', user.token, {path: '/'});
    return {
        type: 'USER_FETCH_DATA_SUCCESS',
        user
    };
}

export function isAdmin(bool = false) {
    return {
        type: 'IS_ADMIN',
        bool
    };
}

export function isLogin(bool = false) {
    return {
        type: 'IS_LOGIN',
        bool
    };
}

export function userFetchData(path, form = undefined) {
    let URL = BASE_URL + path;

    let token = cookie.load('token');

    return (dispatch) => {
        dispatch(userHasError(false));
        let state = false;

        let myHeaders = new Headers();

        if (form) {
            dispatch(userIsLoading(true));
            myHeaders.append('Authorization', "Basic " + btoa(form.user + ":" + form.pass));
            state = 'form';
        }
        else if (token) {
            myHeaders.append('Authorization', 'Bearer ' + token);
            state = 'token';
        }

        if (state) {
            fetch(URL, {headers: myHeaders})
                .then((response) => {
                    if (!response.ok) {
                        throw Error(response.statusText);
                    }

                    dispatch(userIsLoading(false));
                    return response;
                })
                .then((response) => response.json())
                .then((user) => {
                    dispatch(userFetchDataSuccess(user));
                    dispatch(isLogin(true));

                    let s = user.roles.findIndex((el) => {return el === 'admin'});
                    if (s !== -1) {
                        dispatch(isAdmin(true));
                    }
                    else {
                        dispatch(isAdmin(false));
                    }
                })
                .catch(() => {
                    dispatch(isLogin(false));
                    dispatch(userHasError(true));
                    dispatch(userIsLoading(false));
                    if (state === 'form') {
                        dispatch(loginError(true));
                        dispatch(loginError(false));
                    }
                });
        }
    };
}


export function register(data) {
    let url = BASE_URL + REGISTER_PATH;

    return (dispatch) => {
        dispatch(createUserIsLoading(true));

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
        })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(createUserIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((p) => {
                dispatch(userFetchData(USER_PATH, {user: data.email, pass: data.password}));
                dispatch(createUserSuccess(true));
                dispatch(createUserSuccess(false));
            })
            .catch((response) => dispatch(createUserHasError(true)));
    };

}

