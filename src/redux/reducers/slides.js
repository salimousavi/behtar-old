export function slideHasError(state = false, action) {
    switch (action.type) {
        case 'SLIDE_HAS_ERROR':
            return action.hasError;

        default:
            return state;
    }
}

export function slideIsLoading(state = false, action) {
    switch (action.type) {
        case 'SLIDE_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function slide(state = [], action) {
    switch (action.type) {
        case 'SLIDE_FETCH_DATA_SUCCESS':
            return action.slide;

        default:
            return state;
    }
}

export function slidesHasError(state = false, action) {
    switch (action.type) {
        case 'SLIDES_HAS_ERROR':
            return action.hasError;

        default:
            return state;
    }
}

export function slidesIsLoading(state = false, action) {
    switch (action.type) {
        case 'SLIDES_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function slides(state = [], action) {
    switch (action.type) {
        case 'SLIDES_FETCH_DATA_SUCCESS':
            return action.slides;

        default:
            return state;
    }
}

export function createSlideHasError(state = false, action) {
    switch (action.type) {
        case 'CREATE_SLIDE_HAS_ERROR':
            return action.createHasError;

        default:
            return state;
    }
}

export function createSlideIsLoading(state = false, action) {
    switch (action.type) {
        case 'CREATE_SLIDE_IS_LOADING':
            return action.createIsLoading;

        default:
            return state;
    }
}

export function createSlideSuccess(state = false, action) {
    switch (action.type) {
        case 'CREATE_SLIDE_SUCCESS':
            return action.createSuccess;

        default:
            return state;
    }
}
