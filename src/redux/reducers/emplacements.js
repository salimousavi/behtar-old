export function emplacementsHasError(state = false, action) {
    switch (action.type) {
        case 'EMPLACEMENTS_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function emplacementsIsLoading(state = false, action) {
    switch (action.type) {
        case 'EMPLACEMENTS_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function emplacements(state = [], action) {
    switch (action.type) {
        case 'EMPLACEMENTS_FETCH_DATA_SUCCESS':
            return action.emplacements;

        default:
            return state;
    }
}
