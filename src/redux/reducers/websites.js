export function websitesHasError(state = false, action) {
    switch (action.type) {
        case 'WEBSITES_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function websitesIsLoading(state = false, action) {
    switch (action.type) {
        case 'WEBSITES_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function websites(state = [], action) {
    switch (action.type) {
        case 'WEBSITES_FETCH_DATA_SUCCESS':
            return action.websites;

        default:
            return state;
    }
}
