export function contentsType(state = false, action) {
    switch (action.type) {
        case 'CONTENTS_TYPE':
            return action.contentsType;

        default:
            return state;
    }
}

export function contentsHasError(state = false, action) {
    switch (action.type) {
        case 'CONTENTS_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function contentsIsLoading(state = false, action) {
    switch (action.type) {
        case 'CONTENTS_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function contents(state = [], action) {
    switch (action.type) {
        case 'CONTENTS_FETCH_DATA_SUCCESS':
            return action.contents;

        default:
            return state;
    }
}
