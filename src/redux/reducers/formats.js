export function formatsHasError(state = false, action) {
    switch (action.type) {
        case 'FORMATS_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function formatsIsLoading(state = false, action) {
    switch (action.type) {
        case 'FORMATS_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function formats(state = [], action) {
    switch (action.type) {
        case 'FORMATS_FETCH_DATA_SUCCESS':
            return action.formats;

        default:
            return state;
    }
}
