export function loginError(state = false, action) {
    switch (action.type) {
        case 'LOGIN_ERROR':
            return action.loginError;

        default:
            return state;
    }
}

export function userHasError(state = false, action) {
    switch (action.type) {
        case 'USER_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function userIsLoading(state = false, action) {
    switch (action.type) {
        case 'USER_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function user(state = [], action) {
    switch (action.type) {
        case 'USER_FETCH_DATA_SUCCESS':
            return action.user;

        default:
            return state;
    }
}


export function createUserHasError(state = false, action) {
    switch (action.type) {
        case 'CREATE_USER_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function createUserIsLoading(state = false, action) {
    switch (action.type) {
        case 'CREATE_USER_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function createUserSuccess(state = false, action) {
    switch (action.type) {
        case 'CREATE_USER_SUCCESS':
            return action.createSuccess;

        default:
            return state;
    }
}

export function isAdmin(state = false, action) {
    switch (action.type) {
        case 'IS_ADMIN':
            return action.bool;

        default:
            return state;
    }
}


export function isLogin(state = false, action) {
    switch (action.type) {
        case 'IS_LOGIN':
            return action.bool;

        default:
            return state;
    }
}
