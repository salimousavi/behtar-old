export function favoriteHasError(state = false, action) {
    switch (action.type) {
        case 'FAVORITE_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function favoriteIsLoading(state = false, action) {
    switch (action.type) {
        case 'FAVORITE_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function favorite(state = [], action) {
    switch (action.type) {
        case 'FAVORITE_FETCH_DATA_SUCCESS':
            return action.favorite;

        default:
            return state;
    }
}
