export function packageHasError(state = false, action) {
    switch (action.type) {
        case 'PACKAGE_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function packageIsLoading(state = false, action) {
    switch (action.type) {
        case 'PACKAGE_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function createPackageSuccess(state = false, action) {
    switch (action.type) {
        case 'CREATE_PACKAGE_SUCCESS':
            return action.createSuccess;

        default:
            return state;
    }
}

export function _package(state = {contents: []}, action) {
    switch (action.type) {
        case 'PACKAGE_FETCH_DATA_SUCCESS':
            return action.p;

        default:
            return state;
    }
}

export function createPackageHasError(state = false, action) {
    switch (action.type) {
        case 'CREATE_PACKAGE_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function createPackageIsLoading(state = false, action) {
    switch (action.type) {
        case 'CREATE_PACKAGE_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}