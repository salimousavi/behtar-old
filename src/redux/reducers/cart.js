export function cartHasError(state = false, action) {
    switch (action.type) {
        case 'CART_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function cartIsLoading(state = false, action) {
    switch (action.type) {
        case 'CART_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function addToCartHasError(state = false, action) {
    switch (action.type) {
        case 'ADD_TO_CART_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function addToCartIsLoading(state = false, action) {
    switch (action.type) {
        case 'ADD_TO_CART_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function cart(state = [], action) {
    switch (action.type) {
        case 'CART_FETCH_DATA_SUCCESS':
            return action.cart;

        default:
            return state;
    }
}
