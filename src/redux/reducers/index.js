import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { contents, contentsHasError, contentsIsLoading, contentsType } from './contents';
import { packageHasError, packageIsLoading, _package, createPackageHasError, createPackageIsLoading, createPackageSuccess } from './package';
import { packages, packagesHasError, packagesIsLoading } from './packages';
import { content, contentHasError, contentIsLoading, createContentHasError, createContentIsLoading, createContentSuccess,
    deleteContentHasError, deleteContentIsLoading, deleteContentSuccess} from './content';
import { comments, commentsHasError, commentsIsLoading, createCommentsHasError, createCommentsIsLoading } from './comments';
import { formats, formatsHasError, formatsIsLoading} from './formats';
import { websites, websitesHasError, websitesIsLoading} from './websites';
import { categories, categoriesHasError, categoriesIsLoading} from './categories';
import { emplacements, emplacementsHasError, emplacementsIsLoading} from './emplacements';
import { user, userHasError, userIsLoading, createUserHasError, createUserIsLoading, createUserSuccess, isAdmin, isLogin, loginError} from './user';
import { cart, cartHasError, cartIsLoading, addToCartHasError, addToCartIsLoading} from './cart';
import { pageIsAdmin } from './page'
import { favorite, favoriteIsLoading, favoriteHasError } from './favorite'
import { slide, slideHasError, slideIsLoading, slides, slidesIsLoading, slidesHasError, createSlideHasError, createSlideIsLoading, createSlideSuccess } from './slides'
import { users, usersHasError, usersIsLoading, changeStatusUsersHasError, changeStatusUsersIsLoading } from './users'

export default combineReducers({
    form: formReducer,
    contents,
    contentsHasError,
    contentsIsLoading,
    contentsType,
    content,
    contentHasError,
    contentIsLoading,
    createContentHasError,
    createContentIsLoading,
    createContentSuccess,
    deleteContentHasError,
    deleteContentIsLoading,
    deleteContentSuccess,
    comments,
    commentsHasError,
    commentsIsLoading,
    createCommentsHasError,
    createCommentsIsLoading,
    packages,
    packagesHasError,
    packagesIsLoading,
    packageHasError,
    packageIsLoading,
    _package,
    createPackageHasError,
    createPackageIsLoading,
    createPackageSuccess,
    formats,
    formatsHasError,
    formatsIsLoading,
    user,
    userHasError,
    userIsLoading,
    loginError,
    createUserHasError,
    createUserIsLoading,
    createUserSuccess,
    isAdmin,
    isLogin,
    websites,
    websitesHasError,
    websitesIsLoading,
    emplacements,
    emplacementsHasError,
    emplacementsIsLoading,
    cart,
    cartHasError,
    cartIsLoading,
    addToCartHasError,
    addToCartIsLoading,
    pageIsAdmin,
    favorite,
    favoriteIsLoading,
    favoriteHasError,
    slides,
    slidesIsLoading,
    slidesHasError,
    users,
    usersHasError,
    usersIsLoading,
    changeStatusUsersHasError,
    changeStatusUsersIsLoading,
    categories,
    categoriesHasError,
    categoriesIsLoading,
    createSlideHasError,
    createSlideIsLoading,
    createSlideSuccess,
    slide,
    slideHasError,
    slideIsLoading,
});
