export function pageIsAdmin(state = false, action) {
    switch (action.type) {
        case 'PAGE_IS_ADMIN':
            return action.pageIsAdmin;

        default:
            return state;
    }
}