export function deleteContentHasError(state = false, action) {
    switch (action.type) {
        case 'DELETE_CONTENT_HAS_ERRORED':
            return action.deleteHasError;

        default:
            return state;
    }
}

export function deleteContentIsLoading(state = false, action) {
    switch (action.type) {
        case 'DELETE_CONTENT_IS_LOADING':
            return action.deleteIsLoading;

        default:
            return state;
    }
}

export function deleteContentSuccess(state = false, action) {
    switch (action.type) {
        case 'DELETE_CONTENT_SUCCESS':
            return action.deleteSuccess;

        default:
            return state;
    }
}

export function createContentHasError(state = false, action) {
    switch (action.type) {
        case 'CREATE_CONTENT_HAS_ERRORED':
            return action.createHasError;

        default:
            return state;
    }
}

export function createContentIsLoading(state = false, action) {
    switch (action.type) {
        case 'CREATE_CONTENT_IS_LOADING':
            return action.createIsLoading;

        default:
            return state;
    }
}

export function createContentSuccess(state = false, action) {
    switch (action.type) {
        case 'CREATE_CONTENT_SUCCESS':
            return action.createSuccess;

        default:
            return state;
    }
}

export function contentHasError(state = false, action) {
    switch (action.type) {
        case 'CONTENT_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function contentIsLoading(state = false, action) {
    switch (action.type) {
        case 'CONTENT_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function content(state = [], action) {
    switch (action.type) {
        case 'CONTENT_FETCH_DATA_SUCCESS':
            return action.content;

        default:
            return state;
    }
}
