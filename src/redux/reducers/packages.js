export function packagesHasError(state = false, action) {
    switch (action.type) {
        case 'PACKAGES_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export function packagesIsLoading(state = false, action) {
    switch (action.type) {
        case 'PACKAGES_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export function packages(state = [], action) {
    switch (action.type) {
        case 'PACKAGES_FETCH_DATA_SUCCESS':
            return action.packages;

        default:
            return state;
    }
}
