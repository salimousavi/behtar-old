export function getType(tid) {
    switch (tid) {
        case '10':
            return 'AUDIO';

        case '9':
            return 'VIDEO';

        case '5':
        case '6':
        case '7':
        case '8':
            return 'GRAPHICS';

        default:
    }
}