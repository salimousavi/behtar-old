import store from '../redux/store/store';
import {TYPES} from './Constants';

function inArray(needle, haystack) {
    let length = haystack.length;
    for(let i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

export function getCategoryName(id) {
    let states = store.getState();
    let category = states.categories.find((el) => { return el.value == id });

    if (category) return category.label;

    return '        &nbsp;&nbsp;'
}

export function getTypeName(id) {
    let category = TYPES.find((el) => { return el.id == id });
    if (category) return category.name;

    return ''
}

export function getFormatName(ids, output = 'string') {
    let states = store.getState();
    let objects = states.formats.filter((el) => { return inArray(el.value, ids) });

    let e = objects.map(el => {return el.label});

    if (output === 'string') return e.join('، ');

    return e;
}

export function getEmplacementsName(ids, output = 'string') {
    let states = store.getState();
    let objects = states.emplacements.filter((el) => { return inArray(el.value, ids) });

    let e = objects.map(el => {return el.label});

    if (output === 'string') return e.join('، ');

    return e;
}

export function getWebsitesName(ids, output = 'string') {
    let states = store.getState();
    let objects = states.websites.filter((el) => { return inArray(el.value, ids) });

    let e = objects.map(el => {return el.label});

    if (output === 'string') return e.join('، ');

    return e;
}