import cookie from 'react-cookies';

export function Header(options = undefined) {
    let token = cookie.load('token');
    let myHeaders = new Headers();

    if (token !== 'undefined' && token !== undefined) {
        myHeaders.append('Authorization', 'Bearer ' + token);
    }

    if (options) {
        options.headers = myHeaders;
        return options;
    }

    return {headers: myHeaders};
}

export function Token() {
    let token = cookie.load('token');
    if (token !== 'undefined' && token !== undefined) {
        return token;
    }

    return undefined;
}

export function getTokenObject() {
    let token = Token();
    if (token !== undefined) {
        return {Authorization: 'Bearer ' + Token()}
    }
}
