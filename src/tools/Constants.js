module.exports = {

    BASE_URL: "https://20500.ir",
    // BASE_URL: "http://behtar.back",
    PACKAGES_PATH: "/api/packages",
    REGISTER_PATH: "/api/register",
    CART_PATH: "/api/cart",
    ADD_TO_CART_PATH: "/api/cart",
    CONTENTS_PATH: "/api/contents",
    FILES_PATH: "/api/files",
    CONTENT_PATH: "/api/content",
    USER_PATH: "/api/user",
    USERS_PATH: "/api/users",
    FORMATS_PATH: "/api/formats",
    EMPLACEMENTS_PATH: "/api/emplacements",
    WEBSITES_PATH: "/api/websites",
    CATEGORIES_PATH: "/api/categories",
    COMMENTS_PATH: "/api/comments",
    UPLOAD_PATH: "/api/upload",
    FAVORITE_PATH: "/api/favorite",
    SLIDES_PATH: "/api/slides",
    PAGE_CATEGORIES: {
        AUDIOS: "6",
        VIDEOS: "7",
        // GRAPHICS: "1-2",
        LEAFLET: "5",
        IDEAS: "3",
        JOURNAL: "4",
        POSTER: "1",
        LITERATURE: "2",
    },
    TYPES: [
        {
            type: 'poster',
            id: 1,
            category: 'POSTER',
            name: 'پوستر',
            fields: ['Size', 'Layer'],
            filters: ['SizeRange', 'Layer'],
            file_formats: ['jpg', 'jpeg', 'psd', 'tif']
        },
        {
            type: 'literature',
            id: 2,
            category: 'LITERATURE',
            name: 'متن نوشته',
            fields: ['Size', 'Colorful'],
            filters: ['SizeRange', 'Colorful'],
            file_formats: ['jpg', 'jpeg']
        },
        {
            type: 'idea',
            id: 3,
            category: 'IDEAS',
            name: 'ایده',
            fields: ['Implemented', 'Expensive'],
            filters: ['Implemented', 'Expensive'],
            file_formats: ['jpg', 'jpeg', 'pdf', 'doc', 'docs']
        },
        {
            type: 'journal',
            id: 4,
            category: 'JOURNAL',
            name: 'نشریه',
            fields: ['Format', 'Pages', 'Colorful'],
            filters: ['Format', 'PagesRange', 'Colorful'],
            file_formats: ['jpg', 'jpeg', 'pdf', 'doc', 'docs', 'psd', 'tif']
        },
        {
            type: 'leaflet',
            id: 5,
            category: 'LEAFLET',
            name: 'بروشور و جزوه',
            fields: ['Format', 'Pages', 'Colorful'],
            filters: ['Format', 'PagesRange', 'Colorful'],
            file_formats: ['jpg', 'jpeg', 'pdf', 'doc', 'docs', 'psd', 'tif']
        },
        {
            type: 'audio',
            id: 6,
            category: 'AUDIOS',
            name: 'صوت',
            fields: ['Duration', 'Audio'],
            filters: ['DurationRange'],
            file_formats: ['mp3']
        },
        {
            type: 'video',
            id: 7,
            category: 'VIDEOS',
            name: 'فیلم',
            fields: ['Duration', 'Video'],
            filters: ['DurationRange'],
            file_formats: ['mp4']
        },
    ]

};